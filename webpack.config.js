/*global __dirname*/
var CopyWebpackPlugin = require('copy-webpack-plugin');
var UglifyJSPlugin = require('uglifyjs-webpack-plugin');


module.exports = {
    context: __dirname + '/src',
    entry: __dirname + '/src/fantasydraft/init.jsx',
    output: {
        path: __dirname + '/release',
        filename: 'application.js'
    },
    module: {
        loaders: [
            {
                test: /\.jsx?$/,
                exclude: /(node_modules)/,
                loader: 'babel-loader',
                query: {
                    presets: ['env', 'react']
                }
            },
            {
                test: /\.(jpg|png|svg)$/,
                loader: 'file-loader',
                options: {
                    name: '[hash].[ext]'
                }
            }
        ]
    },
    resolve: {
        extensions: ['.js', '.jsx']
    },
    plugins: [
        new CopyWebpackPlugin([
            { from: 'index.html', to: 'index.html' }
        ]),
        // new UglifyJSPlugin({
        //     sourceMap: true
        // })
    ],
    devServer: {
        contentBase: 'release',
        historyApiFallback: true
    }
};
