import React from 'react';


export default class WatchEventPage extends React.Component {
    render() {
        let eventId = this.props.eventId;
        return (
            <div>
                <h1>WATCH ROOM</h1>
                <p>Event ID: { eventId }</p>
                <h1>TODO</h1>
                <ul>
                    <li>Everything</li>
                </ul>
            </div>
        );
    }
}

WatchEventPage.propTypes = {
    eventId: React.PropTypes.string.isRequired
};
