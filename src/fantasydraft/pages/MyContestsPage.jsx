import React from 'react';
import formatTicketCost from '../../utils/formatTicketCost';
import formatTicketAmountwon from '../../utils/formatTicketAmountwon';
import formatPoints from '../../utils/formatPoints';
import { Link } from 'react-router-dom';
import ContextIcon from '../components/ContextIcon';
import InfiniteScroll from '../../components/InfiniteScroll';


class TicketsTable extends InfiniteScroll {
    getRecords() {
        return this.props.tickets;
    }
    renderHeader() {
        return (
            <thead>
                <tr>
                    <th></th>
                    <th>ID</th>
                    <th className="text-left">Contest Name</th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th>PMR</th>
                    <th>Entry Fee</th>
                    <th>Winning</th>
                    <th>Score</th>
                    <th>Rank</th>
                    <th></th>
                </tr>
            </thead>
        );
    }
    renderRecord(ticket) {
        let event = this.props.store.events.byId[ticket.eventId];
        return (
            <tr key={ ticket.id }>
                <td className="xsmall-width"><ContextIcon context={ event.context } /></td>
                <td className="smaller-width">{ event.id }</td>
                <td className="large-width text-left no-border-right"><a>{ event.description }</a></td>
                <td className="icon-width no-border-left no-border-right"></td>
                <td className="icon-width no-border-left no-border-right"></td>
                <td className="icon-width no-border-left no-border-right"></td>
                <td className="icon-width no-border-left no-border-right"></td>
                <td className="small-width">{ ticket.eventScoringTimeIndicator }</td>
                <td className="small-width">{ formatTicketCost(event.ticketCost) }</td>
                <td className={ 'small-width ' + (ticket.amountWon ? 'won ' : '') }>{ formatTicketAmountwon(ticket.amountWon) }</td>
                <td className="small-width">{ formatPoints(ticket.pointsEarned) }</td>
                <td className="small-width">{ ticket.position } / { event.ticketCount }</td>
                <td className="btn-width"><Link className="btn btn-positive btn-block" to={ `/ticket/${ ticket.id }/` }>View</Link></td>
            </tr>
        );
    }
}


export default class MyContestsPage extends React.PureComponent {
    render() {
        let tickets = Object.values(this.props.store.tickets.byId);
        return (
            <div>
                <div className="ogs-tickets">
                    <TicketsTable className="table branded" { ...this.props } tickets={ tickets } />
                </div>
                <h1>TODO</h1>
                <ul>
                    <li>Summary Panel</li>
                    <li>Live tab</li>
                    <li>Upcoming tab</li>
                    <li>Completed tab</li>
                    <li>Auto-selection of tab from left to right</li>
                    <li>Different columns by tab</li>
                    <li>PMR SVG</li>
                    <li>M/G/experience icons for events</li>
                    <li>Event details modal</li>
                    <li>Live ticket count</li>
                    <li>Create a contest</li>
                    <li>Sorting by columns</li>
                    <li>Enhanced prize structure display</li>
                </ul>
            </div>
        )
    }
}
