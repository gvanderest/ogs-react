import React from 'react';
import AccountMenu from '../components/AccountMenu';


export default class DepositLimitsPage extends React.Component {
    render() {
        return (
            <div>
                <AccountMenu { ...this.props } />
                <h1>Deposit Limits</h1>
                <h1>TODO</h1>
                <ul>
                    <li>Self-Exclusion program content</li>
                    <li>Self-exclusion form</li>
                    <li>Deposit limit settings</li>
                    <li>Deposit limit form</li>
                    <li>Thermometer of deposit limit</li>
                    <li>Remaining limit available</li>
                    <li>Timeframe that changes based on daily, weekly, monthly</li>
                    <li>Paragraph that shows if your state's limit is not default</li>
                </ul>
            </div>
        );
    }
}
