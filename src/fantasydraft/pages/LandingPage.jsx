import React from 'react';
import { Link } from 'react-router-dom';
import { push } from 'react-router-redux';


export default class LandingPage extends React.Component {
    componentDidMount() {
        this.checkForRedirect(this.props);
    }
    componentWillReceiveProps(nextProps) {
        this.checkForRedirect(nextProps);

    }
    checkForRedirect(props) {
        let customer = props.store.customer.activeCustomer;
        if (customer) {
            props.actions.router.push('/contests/');
        }
    }
    render() {
        return (
            <div>
                <div className="content-wrapper landing">
                <div className="row top-row">
                <div className="col-sm-10 col-sm-offset-1 text-center home-promo">
                <h1>Every Day is Gameday on FantasyDraft</h1>
                <div className="row">
                <div className="col-md-12">
                <h4 className="tagline tagline-decoration">Daily Fantasy NFL, MLB, NBA, NHL &amp; PGA</h4>
                </div>
                </div>
                <div className="row reasons">
                <div className="col-sm-5 col-sm-offset-1 hidden-xs reasons-video">
                <div className="embed-responsive embed-responsive-16by9">
                <a>
                    <img className="embed-responsive-item" src="//i.ytimg.com/vi/i5AgP2NHqfc/maxresdefault.jpg" />
                    <div className="play-button"></div>
                </a>
                </div>
                </div>
                <div className="col-sm-6 reasons-text">
                    <ul className="landing-bullets">
                        <li><span className="glyphicon glyphicon-play"></span> No season-long commitments</li>
                        <li><span className="glyphicon glyphicon-play"></span> Draft new teams daily</li>
                        <li><span className="glyphicon glyphicon-play"></span> Sign up for free</li>
                        <li><span className="glyphicon glyphicon-play"></span> Win real cash prizes</li>
                    </ul>
                <div className="row">
                <div className="col-sm-8 col-md-8 col-lg-6 col-xl-3">
                <a className="btn btn-positive" href="/contests">
                Play Now »</a>
                </div>
                </div>
                </div>
                </div>
                </div>
                </div>
                <div className="row differentiators">
                <div className="row visible-xs pf-vid">
                <div className="col-xs-1">
                </div>
                <div className="col-xs-10">
                <div className="embed-responsive embed-responsive-16by9">
                <a>
                <img className="embed-responsive-item" src="//i.ytimg.com/vi/i5AgP2NHqfc/maxresdefault.jpg" />
                <div className="play-button">
                </div>
                </a>
                </div>
                </div>
                </div>
                <div className="col-md-8 col-md-offset-2 text-center">
                    <div className="row text-center">
                        <h2>FantasyDraft puts #PlayersFirst</h2>
                        <div className="mission">Our mission is to provide a fun &amp; fair experience for <em>all</em> players</div>
                    </div>
                <div className="row diffs">
                <div className="col-md-4 col-sm-6">
                <h3>Larger PayoutZone</h3>
                A minimum of 25%* of players win in our guaranteed contests. A larger PayoutZone means more chances to win!</div>
                <div className="col-md-4 col-sm-6">
                <h3>
                Multi-entry Capping</h3>
                We level the playing field by maintaining a low entry cap in our contests.</div>
                <div className="clearfix visible-sm">
                </div>
                <div className="col-md-4 col-sm-6">
                <h3>
                Lower Contests Fees</h3>
                Lower contest fees mean more of your entry fees go toward the PayoutZone.</div>
                <div className="clearfix visible-md visible-lg">
                </div>
                <div className="col-md-4 col-sm-6">
                <h3>
                Flexible Lineups</h3>
                We give you the freedom to choose more of your favorite players.</div>
                <div className="clearfix visible-sm">
                </div>
                <div className="col-md-4 col-sm-6">
                <h3>
                6 Degrees of Pay℠</h3>
                Earn commissions from the contest fees paid by players you refer, and the players they refer across 6 total tiers.</div>
                <div className="col-md-4 col-sm-6">
                <h3>
                Play With the Pros</h3>
                Play against pro athletes and sports personalities both active and retired, and see how you stack up!</div>
                <div className="clearfix visible-sm">
                </div>
                <div className="clearfix visible-md visible-lg">
                </div>
                </div>
                <div className="row text-center">
                <p>
                <a className="btn btn-positive" href="/contests">
                Play Now »</a>
                </p>
                </div>
                </div>
                </div>
                <div className="row locker-room">
                <div className="col-md-4 col-md-offset-2 lr-left">
                <a target="_blank" href="https://lockerroom.fantasydraft.com/">
                    <img src="https://d1oq8jvxue05xu.cloudfront.net/assets/img/locker-room-logo-for-list.png" />
                </a>
                 <br />
                <p>
                The place to find expert analysis, insights and strategy guides designed to make your time playing on FantasyDraft more fun and productive.</p>
                <p>
                <a href="https://lockerroom.fantasydraft.com/category/londons-line/" target="_blank">
                <strong>Exclusive content from retired NFL Iron Man London Fletcher&nbsp;»</strong>
                </a>
                </p>
                <p>
                <a href="https://lockerroom.fantasydraft.com/category/fantasydraft-101/" target="_blank">
                <strong>Brush up on your daily fantasy skills with our FantasyDraft 101 series&nbsp;»</strong>
                </a>
                </p>
                </div>
                <div className="col-lg-4 col-md-4">
                    <h2>Recent Articles</h2>
                    <div id="lr-content">
                        <div></div>
                    </div>
                </div>
                </div>
                <div className="row disclaimer">
                <div className="text-center col-sm-6 col-sm-offset-3 diff-disclaimer">*Qualifier, VIP experience, championship, special event, and user-created contests <br className="hidden-xs hidden-sm" />may award prizes to top finishers only.</div>
                </div>
                </div>
            </div>
        );
    }
}
