import React from 'react';
import YouTube from '../../components/YouTube';


export default function ReferralsPage() {
    return (
        <div className="row">
            <div className="col-md-5">
                <div className="row">
                    <div className="col-md-12">
                        <h2>Referral Basics</h2>
                        <YouTube video="89L5aEgKlX8" />
                    </div>
                </div>
                <div className="button-holder-block" ng-hide="customer">
                    <div className="row">
                        <div className="col-md-6 col-md-offset-3">
                            <a className="btn btn-block btn-positive" ng-click="clickRegister()">Join Now</a>
                        </div>
                    </div>
                </div>
                <div className="row vspace-20">
                    <div className="col-md-12 vspace-20">
                        <h4>Frequent 6 Degrees of Pay&#8480; &nbsp;Questions</h4>
                    </div>
                    <div className="col-md-12 vspace-20"></div>
                </div>
            </div>
            <div className="col-md-7">
                <div className="row">
                    <div className="col-lg-12 vspace-10">
                        <h4>FantasyDraft's 6 Degrees of Pay&#8480; &nbsp;Referral Program</h4>
                    </div>
                </div>
                <div className="row small-gutter vspace-10">
                    <div className="col-md-8 col-sm-8">
                        <p>When we created the FantasyDraft referral program we wanted to build something that was unique, groundbreaking and unmatched in the daily fantasy sports industry. With that in mind we built the industry's only tiered pay-for-play referral program, we call it FantasyDraft's 6 Degrees of Pay&#8480;.</p>
                        <p>What can 6 Degrees of Pay&#8480; do for you? Refer your friends to FantasyDraft and make cash EVERY time they pay to play. That in itself is not a new concept, here is where we separate ourselves from the pack. Not only will we pay you for the players you refer but we will also pay you for the players they refer and so on 5 times removed.</p>
                        <p>We set aside approximately one third of contest fees to be paid to FantasyDraft players hereafter referred to as 6 Degrees of Pay Commissions&#8480;. The contest fee is the portion of the contest entry fee that FantasyDraft keeps to run the business. We then pay you a percentage of your referrals 6 Degrees of Pay Commissions&#8480; based on the tiers below.</p>
                    </div>
                    <div className="col-md-4 col-sm-4 hidden-xs">
                        <img className="img-responsive" ng-src="https://d10jvztjcs209c.cloudfront.net/assets/img/6degrees.png" src="https://d10jvztjcs209c.cloudfront.net/assets/img/6degrees.png" />
                    </div>
                </div>
                <div className="row vspace-20">
                    <div className="col-md-12">
                        <h4>6 Degrees of Pay Commissions&#8480; &nbsp;Payout Tiers</h4>
                        <ig-refer-payout-table>
                            <div className="refer-payout-table">
                                <table className="table branded headless">
                                    <tbody>
                                        <tr ng-repeat="percent in ['10%', '10%', '15%', '15%', '20%', '30%'] track by $index">
                                            <td className="highlighted no-break">1st&deg;</td>
                                            <td className="text-center" ng-if="$index == 0">Personally referred players.</td>
                                            <td className="text-center"><h2>10%</h2></td>
                                            <td className="text-left"><strong>of 6 Degrees of Pay Commissions</strong></td>
                                        </tr>
                                        <tr ng-repeat="percent in ['10%', '10%', '15%', '15%', '20%', '30%'] track by $index">
                                            <td className="highlighted no-break">2nd&deg;</td>
                                            <td className="text-center" ng-if="$index > 0">Players referred by your 1st Degree.</td>
                                            <td className="text-center"><h2>10%</h2></td>
                                            <td className="text-left"><strong>of 6 Degrees of Pay Commissions</strong></td>
                                        </tr>
                                        <tr ng-repeat="percent in ['10%', '10%', '15%', '15%', '20%', '30%'] track by $index">
                                            <td className="highlighted no-break">3rd&deg;</td>
                                            <td className="text-center" ng-if="$index > 0">Players referred by your 2nd Degree.</td>
                                            <td className="text-center"><h2>15%</h2></td>
                                            <td className="text-left"><strong>of 6 Degrees of Pay Commissions</strong></td>
                                        </tr>
                                        <tr ng-repeat="percent in ['10%', '10%', '15%', '15%', '20%', '30%'] track by $index">
                                            <td className="highlighted no-break">4th&deg;</td>
                                            <td className="text-center" ng-if="$index > 0">Players referred by your 3rd Degree.</td>
                                            <td className="text-center"><h2>15%</h2></td>
                                            <td className="text-left"><strong>of 6 Degrees of Pay Commissions</strong></td>
                                        </tr>
                                        <tr ng-repeat="percent in ['10%', '10%', '15%', '15%', '20%', '30%'] track by $index">
                                            <td className="highlighted no-break">5th&deg;</td>
                                            <td className="text-center" ng-if="$index > 0">Players referred by your 4th Degree.</td>
                                            <td className="text-center"><h2>20%</h2></td>
                                            <td className="text-left"><strong>of 6 Degrees of Pay Commissions</strong></td>
                                        </tr>
                                        <tr ng-repeat="percent in ['10%', '10%', '15%', '15%', '20%', '30%'] track by $index">
                                            <td className="highlighted no-break">6th&deg;</td>
                                            <td className="text-center" ng-if="$index > 0">Players referred by your 5th Degree.</td>
                                            <td className="text-center"><h2>30%</h2></td>
                                            <td className="text-left"><strong>of 6 Degrees of Pay Commissions</strong></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </ig-refer-payout-table>
                    </div>
                </div>
            </div>
            <h1>TODO</h1>
            <ul>
                <li>Content</li>
                <li>Expanding items</li>
                <li>Join now button</li>
                <li>YouTube embed</li>
            </ul>
        </div>
    );
}
/*
                    <ig-accordion-list class="alt-accordion">
                        <div className="accordion-list" ng-transclude="">
                            <ig-accordion-section title="What is the best way to refer someone?">
                                <div className="accordion-section">
                                    <div className="heading" ng-click="clickHeading()">
                                        <i ng-class="icon_classes" className="fa fa-plus-circle"></i>
                                        <h2 className="title" ng-bind-html="title">What is the best way to refer someone?</h2>
                                        <div className="icons" ng-if="icons" ng-click="$event.stopPropagation()"></div>
                                    </div>
                                    <div className="content clearfix collapsed" ng-class="{ expanded: active, collapsed: !active }" ng-transclude="">
                                        <p>By far the easiest way to refer someone is through the tools provided in your My Referrals section. Here you can share your unique referral link on social media sites, copy your link to promote elsewhere or use our email tool and we will send out the link for you.</p>
                                    </div>
                                </div>
                            </ig-accordion-section>
                            <ig-accordion-section title="Can I email my referrals myself?">
                                <div className="accordion-section">
                                    <div className="heading" ng-click="clickHeading()">
                                        <i ng-class="icon_classes" className="fa fa-plus-circle"></i>
                                        <h2 className="title" ng-bind-html="title">Can I email my referrals myself?</h2>
                                        <div className="icons" ng-if="icons" ng-click="$event.stopPropagation()"></div>
                                    </div>
                                    <div className="content clearfix collapsed" ng-class="{ expanded: active, collapsed: !active }" ng-transclude="">
                                        <p>Yes, you can send your own email if you don't want us to send them on your behalf. Copy your custom link from your My Referrals and paste it in any email. Anyone that signs up from that link will automatically be included in your referral network.</p>
                                    </div>
                                </div>
                            </ig-accordion-section>
                    <ig-accordion-section title="My friend didn't use my name when they signed up, can I get him in my network?">
                    <div className="accordion-section">
                    <div className="heading" ng-click="clickHeading()">
                    <i ng-class="icon_classes" className="fa fa-plus-circle">
                    </i>
                    <h2 className="title" ng-bind-html="title">My friend didn't use my name when they signed up, can I get him in my network?</h2>
                    <div className="icons" ng-if="icons" ng-click="$event.stopPropagation()">
                    </div>
                    </div>
                    <div className="content clearfix collapsed" ng-class="{ expanded: active, collapsed: !active }" ng-transclude="">
                    <p>If your friend just signed up, have him contact Customer Support requesting that he be added to your network. Once we confirm with your friend and you, we will add them to your network. Keep in mind that we only allow a user to request a change to their referee status once and it must be within the first 2 days of registering.</p>
                    </div>
                    </div>
                    </ig-accordion-section>
                    <ig-accordion-section title="When will I receive my 6 Degrees of Pay Commissions&#8480;?">
                    <div className="accordion-section">
                    <div className="heading" ng-click="clickHeading()">
                        <i ng-class="icon_classes" className="fa fa-plus-circle"></i>
                        <h2 className="title" ng-bind-html="title">When will I receive my 6 Degrees of Pay Commissions&#8480;?</h2>
                    <div className="icons" ng-if="icons" ng-click="$event.stopPropagation()">
                    </div>
                    </div>
                    <div className="content clearfix collapsed" ng-class="{ expanded: active, collapsed: !active }" ng-transclude="">
                    <p>6 Degrees of Pay Commissions&#8480; will be credited to your account upon completion of the contests in which your referrals have played. As is the case with all contests once the official box score is published the results will become final and your 6 Degrees of Pay Commissions&#8480; will be deposited into your account.</p>
                    </div>
                    </div>
                    </ig-accordion-section>
                    <ig-accordion-section title="Can I withdraw my 6 Degrees of Pay Commissions&#8480;?">
                    <div className="accordion-section">
                    <div className="heading" ng-click="clickHeading()">
                        <i ng-class="icon_classes" className="fa fa-plus-circle"></i>
                        <h2 className="title" ng-bind-html="title">Can I withdraw my 6 Degrees of Pay Commissions&#8480;?</h2>
                    <div className="icons" ng-if="icons" ng-click="$event.stopPropagation()">
                    </div>
                    </div>
                    <div className="content clearfix collapsed" ng-class="{ expanded: active, collapsed: !active }" ng-transclude="">
                    <p>Yes, 6 Degrees of Pay Commissions&#8480; are just like winnings and thus are treated same as cash. They can be withdrawn as soon as they are posted to your account.</p>
                    </div>
                    </div>
                    </ig-accordion-section>
                    <ig-accordion-section title="Are my 6 Degrees of Pay Commissions&#8480; taxable?">
                    <div className="accordion-section">
                    <div className="heading" ng-click="clickHeading()">
                    <i ng-class="icon_classes" className="fa fa-plus-circle">
                    </i>
                    <h2 className="title" ng-bind-html="title">Are my 6 Degrees of Pay Commissions&#8480; taxable?</h2>
                    <div className="icons" ng-if="icons" ng-click="$event.stopPropagation()">
                    </div>
                    </div>
                    <div className="content clearfix collapsed" ng-class="{ expanded: active, collapsed: !active }" ng-transclude="">
                    <p>Yes, Uncle Sam considers this taxable income. Therefore if your annual winnings (winnings minus entry fees) plus your 6 Degrees of Pay Commissions&#8480; exceed $600 in a calendar year you will be required to complete a 1099-MISC.</p>
                    </div>
                    </div>
                    </ig-accordion-section>
                    <ig-accordion-section title="Can I earn 6 Degrees of Pay Commissions&#8480; if I live in one of the 'restricted states'?">
                    <div className="accordion-section">
                    <div className="heading" ng-click="clickHeading()">
                    <i ng-class="icon_classes" className="fa fa-plus-circle"></i>
                    <h2 className="title" ng-bind-html="title">Can I earn 6 Degrees of Pay Commissions&#8480; if I live in one of the 'restricted states'?</h2>
                    <div className="icons" ng-if="icons" ng-click="$event.stopPropagation()">
                    </div>
                    </div>
                    <div className="content clearfix collapsed" ng-class="{ expanded: active, collapsed: !active }" ng-transclude="">
                    <p>Yes, FantasyDraft 6 Degrees of Pay Commissions&#8480; are available to individuals living in restricted states. As you know you can't play cash games or win prizes but the good news is you can earn 6 Degrees of Pay Commissions&#8480;, all is not lost!</p>
                    </div>
                    </div>
                    </ig-accordion-section>
                    <ig-accordion-section title="Which entry fees are eligible for 6 Degrees of Pay Commissions&#8480;?">
                    <div className="accordion-section">
                    <div className="heading" ng-click="clickHeading()">
                    <i ng-class="icon_classes" className="fa fa-plus-circle"></i>
                    <h2 className="title" ng-bind-html="title">Which entry fees are eligible for 6 Degrees of Pay Commissions&#8480;?</h2>
                    <div className="icons" ng-if="icons" ng-click="$event.stopPropagation()">
                    </div>
                    </div>
                    <div className="content clearfix collapsed" ng-class="{ expanded: active, collapsed: !active }" ng-transclude="">
                    <p>Cash used to enter a fee based contest are eligible for 6 Degree of Pay Commissions&#8480;. The cash can be from cash winnings, cash deposits, or cash earned from 6 Degrees of Pay Commissions&#8480;. Fantasy Cash, tickets and free entry slots are not eligible and will not be included in your commissions calculations.</p>
                    </div>
                    </div>
                    </ig-accordion-section>
                    </div>
                    </ig-accordion-list>
*/
