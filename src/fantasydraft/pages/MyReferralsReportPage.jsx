import React from 'react';


export default class MyReferralsReportPage extends React.Component {
    render() {
        return (
            <div>
                <h1>My Referrals Group Report</h1>
                <h1>TODO</h1>
                <ul>
                    <li>Top menu highlight of My Referrals if on this page</li>
                    <li>Tabs for filtering date range</li>
                    <li>Select box for filtering degrees</li>
                    <li>Pagination</li>
                    <li>Tabular data formatting</li>
                    <li>Sorting by columns</li>
                    <li>Page Textbox that triggers on enter</li>
                    <li>Per page select dropdown</li>
                </ul>
            </div>
        )
    }
}
