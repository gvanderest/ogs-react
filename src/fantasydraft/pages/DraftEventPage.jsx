import _ from 'lodash';
import React from 'react';
import InfiniteScroll from '../../components/InfiniteScroll';
import moment from 'moment';
import formatSelectionCost from '../../utils/formatSelectionCost';

import OutcomeDetailsModal from '../components/OutcomeDetailsModal';
import DraftOutcomesTable from '../components/DraftOutcomesTable';
import DraftSelectionsTable from '../components/DraftSelectionsTable';
import DraftGamesTable from '../components/DraftGamesTable';
import Modal, { ModalHeader, ModalBody, ModalFooter } from '../components/Modal';


export default class DraftEventPage extends React.Component {
    constructor() {
        super()
        this.state = {
            outcomeDetailsId: null,
            selections: [],
            positions: [],
            sortField: '-selectionCost',
            outcomeFilterTabs: [],
            selectedOutcomeFilterTab: null,
            outcomeSearchEnabled: false,
            outcomeSearchText: '',
            selectedTeamIds: [],
            autoAdvance: true,
            positionsFullError: false
        };
    }
    componentDidMount() {
        this.props.actions.draft.startDraftingEvent(this.props.eventId);
    }
    componentWillUnmount() {
        this.props.actions.draft.stopDraftingEvent(this.props.eventId);
    }
    componentWillReceiveProps(nextProps) {
        let store = nextProps.store;
        let eventId: string = nextProps.eventId;
        let event = store.events.byId[eventId];
        let eventGames = store.eventGames.byId[event.externalId];
        let games = (eventGames && eventGames.gameIds) ? eventGames.gameIds.map((gameId) => {
            let game = {
                ...store.games.byId[gameId]
            };
            game.homeTeam = store.teams.byId[game.homeTeamId];
            game.visitingTeam = store.teams.byId[game.visitingTeamId];
            return game;
        }) : [];

        let positions = (eventGames && eventGames.positionIds) ? eventGames.positionIds.map((positionId) => {
            return {
                ...store.positions.byId[positionId]
            };
        }) : [];
        positions = _.sortBy(positions, ['sortOrder'], ['asc']);

        let outcomes = (eventGames && eventGames.outcomeIds) ? eventGames.outcomeIds.map((outcomeId) => {
            let outcome = {
                ...store.outcomes.byId[outcomeId]
            };
            let player = outcome.player = store.players.byId[outcome.externalId];
            let team = player.team = store.teams.byId[player.teamId];
            if (team) {
                team.game = store.games.byId[team.gameId];
            }
            return outcome;
        }) : [];

        // Used for generating the tabs
        let outcomeFilterTabs = this.generateOutcomeFilterTabs(positions);
        let selectedOutcomeFilterTab = this.state.selectedOutcomeFilterTab;
        if (!selectedOutcomeFilterTab && positions.length > 0) {
            selectedOutcomeFilterTab = positions[0];
        }

        this.setState({
            event,
            eventGames,
            games,
            positions,
            outcomeFilterTabs,
            selectedOutcomeFilterTab,
            outcomes
        });
    }
    generateOutcomeFilterTabs(positions) {
        let usedNames = [];
        let tabs = [];
        _.each(positions, (position) => {
            if (_.includes(usedNames, position.name)) {
                return;
            }

            tabs.push(position);
            usedNames.push(position.name);
        });
        return tabs;
    }
    applySelectOutcomeForPosition(state, outcome, position) {
        let selections = this.state.selections;

        state = {
            ...state,
            selections: [
                ...selections,
                {
                    outcomeId: outcome.id,
                    positionId: position.id
                }
            ]
        };

        state = this.applyAutoAdvance(state);

        return state;
    }
    getSortedPositions() {
        return _.sortBy(this.state.positions, 'sortOrder');
    }
    getFirstAvailablePosition(outcome) {
        let positionIdsUsed = _.map(this.state.selections, 'positionId');
        let sortedPositions = this.getSortedPositions();
        return _.find(sortedPositions, (position) => {
            if (outcome && !_.includes(position.outcomeTypeNames, outcome.typeName)) {
                return false;
            }
            return !_.includes(positionIdsUsed, position.id);
        });
    }
    applySelectOutcome(state, outcome) {
        let firstPosition = this.getFirstAvailablePosition(outcome);
        if (!firstPosition) {
            return {
                positionsFullError: true,
            };
        }
        state = {
            ...state,
            outcomeSearchEnabled: false
        };
        state = this.applySelectOutcomeForPosition(state, outcome, firstPosition);
        return state;
    }
    handleAddOutcome(outcome) {
        let state = this.applySelectOutcome(this.state, outcome);
        this.setState(state);
    }
    applyRemoveSelection(state, selection) {
        state = {
            ...state,
            selections: _.without(this.state.selections, selection)
        };
        state = this.applyAutoAdvance(state);
        return state;
    }
    handleRemoveSelection(selection) {
        this.setState(this.applyRemoveSelection(this.state, selection));
    }
    componentDidUpdate() {
        if (this.state.outcomeSearchEnabled) {
            this.refs.outcomeSearchInput.focus();
        }
    }
    onClickTeam(team) {
        let { selectedTeamIds } = this.state;
        if (_.includes(selectedTeamIds, team.id)) {
            selectedTeamIds = _.without(selectedTeamIds, team.id);
        } else {
            selectedTeamIds = [...selectedTeamIds, team.id];
        }
        this.setState({
            selectedTeamIds
        });
    }
    onClickGame(game) {
        let { selectedTeamIds } = this.state;
        let teamIds = [game.visitingTeam.id, game.homeTeam.id];

        if (_.intersection(selectedTeamIds, teamIds).length === teamIds.length) {
            selectedTeamIds = _.xor(selectedTeamIds, teamIds);
        } else {
            selectedTeamIds = _.union(selectedTeamIds, teamIds);
        }

        this.setState({
            selectedTeamIds
        });
    }
    onClickClearGames() {
        this.setState({
            selectedTeamIds: []
        });
    }
    handleClearSelections() {
        let state = {
            ...this.state,
            selections: []
        };

        state = this.applyAutoAdvance(state);

        this.setState(state);
    }
    applyAutoAdvance(state) {
        if (!state.autoAdvance) {
            return state;
        }
        let selectionsByPositionId = _.keyBy(state.selections, (selection) => {
            return selection.positionId;
        });
        let firstAvailablePosition = _.find(state.positions, (position) => {
            return !selectionsByPositionId[position.id];
        });
        if (firstAvailablePosition) {
            return {
                ...state,
                selectedOutcomeFilterTab: _.find(this.state.outcomeFilterTabs, (tab) => {
                    return tab.name === firstAvailablePosition.name;
                })
            }
        }
        return state;
    }
    setAutoAdvance(enabled) {
        let state = {
            ...this.state,
            autoAdvance: enabled
        };
        state = this.applyAutoAdvance(state);
        this.setState(state);
    }
    handlePositionsFullErrorClose() {
        this.setState({
            positionsFullError: false
        });
    }
    render() {
        let outcomeFilterTabs = this.state.outcomeFilterTabs;
        let selectedOutcomeFilter = this.state.selectedOutcomeFilterTab;

        let { event, eventGames, outcomes, selections, games, positions, selectedTeamIds } = this.state;
        let selectedOutcomeIds = _.map(selections, 'outcomeId');
        let outcomeSearchRegExp = new RegExp(this.state.outcomeSearchText.trim(), 'i');
        let normalizedOutcomeSearchText = this.state.outcomeSearchText.toLowerCase();
        let filteredOutcomes = outcomes;

        filteredOutcomes = _.filter(filteredOutcomes, (outcome) => {
            if (_.includes(selectedOutcomeIds, outcome.id)) {
                return false;
            }
            if (selectedTeamIds.length && !_.includes(selectedTeamIds, outcome.player.team.id)) {
                return false;
            }

            if (this.state.outcomeSearchEnabled) {
                return outcomeSearchRegExp.test(outcome.name);
            } else {
                if (!_.includes(selectedOutcomeFilter.outcomeTypeNames, outcome.typeName)) {
                    return false;
                }
                return true;
            }
        });

        // TODO only sort if the properties change or a sort is requested by user
        // in the componentWillReceiveProps or onClick
        // let sortedOutcomes = _.orderBy(
        //     filteredOutcomes,
        //     [this.state.sortField.replace('-', '')],
        //     [this.state.sortField.indexOf('-') === -1 ? 'asc' : 'desc']
        // );
        let sortedOutcomes = filteredOutcomes;

        let selectionsByPositionId = _.keyBy(selections, 'positionId');
        let emptyPositions = positions.length - selections.length;
        let sumOfSelectionSalaries = _.sum(_.map(selections, (selection) => {
            return this.props.store.outcomes.byId[selection.outcomeId].selectionCost;
        }));
        let remainingSalary = (eventGames ? eventGames.selectionCurrency : 0) - sumOfSelectionSalaries;
        let averageRemainingSalary = emptyPositions === 0 ? 0 : (remainingSalary / emptyPositions);
        return (
            <div>
                { this.state.positionsFullError ? (
                    <Modal onClose={ () => { this.setState({ positionsFullError: false }) } }>
                        <ModalHeader>
                            <h2>Position Full</h2>
                        </ModalHeader>
                        <ModalBody>
                            <p>There are no more slots available for that position, please remove a player or choose a player from a different position.</p>
                        </ModalBody>
                        <ModalFooter>
                            <button type="button" className="btn btn-neutral" onClick={ () => { this.setState({ positionsFullError: false }) } }>Ok</button>
                        </ModalFooter>
                    </Modal>
                ) : null }

                { this.state.outcomeDetailsId ? (
                    <OutcomeDetailsModal
                        outcomeId={ this.state.outcomeDetailsId }
                        onClose={ () => { this.setState({ outcomeDetailsId: null }) } }
                    />
                ) : null }
                <DraftGamesTable
                    games={ games }
                    onClickTeam={ this.onClickTeam.bind(this)  }
                    onClickGame={ this.onClickGame.bind(this) }
                    onClickClear={ this.onClickClearGames.bind(this) }
                    selectedTeamIds={ this.state.selectedTeamIds }
                    onClickGameDepthChart={ (game) => { window.alert('DEPTH CHART ' + game.id) } }
                    onClickGameDetails={ (game) => { window.alert('GAME DETAILS ' + game.id) } }
                    />

                <div className="row vspace-20">
                    <div className="col-md-6">
                        <div className="row">
                            <div className="col-lg-4 col-md-3">
                                <h3 className="table-heading">Player Pool</h3>
                            </div>
                            <div className="col-lg-8 col-md-9">
                                <ul className="list pilled pull-right position-tabs">
                                    <li onClick={ () => { this.setState({ outcomeSearchEnabled: !this.state.outcomeSearchEnabled, outcomeSearchText: '' }) } }>
                                        <a className={ this.state.outcomeSearchEnabled ? 'selected' : '' }>Find <i className="fa fa-angle-down"></i></a>
                                    </li>
                                    { outcomeFilterTabs.map((tab) => {
                                        let selectedTabId = this.state.selectedOutcomeFilterTab ? this.state.selectedOutcomeFilterTab.id : null;
                                        return (
                                            <li key={ tab.id } onClick={ () => { this.setState({ autoAdvance: false, outcomeSearchEnabled: false, selectedOutcomeFilterTab: tab }) } }>
                                                <a className={ (!this.state.outcomeSearchEnabled && selectedTabId === tab.id) ? 'selected' : '' }>{ tab.name }</a>
                                            </li>
                                        );
                                    }) }
                                </ul>
                            </div>
                        </div>
                        { this.state.outcomeSearchEnabled ? (
                            <div className="row">
                                <div className="col-md-12">
                                    <div className="">
                                        <div>
                                            <div className="player-name-search">
                                                <div className="form-group has-feedback no-margin player-search">
                                                    <input
                                                        className="form-control no-input-x"
                                                        onChange={ (e) => { this.setState({ outcomeSearchText: e.target.value }) } }
                                                        value={ this.state.outcomeSearchText }
                                                        placeholder="Enter Player Name"
                                                        type="text"
                                                        ref="outcomeSearchInput"
                                                        />
                                                    { this.state.outcomeSearchText ? (
                                                        <span
                                                            onClick={ (e) => { this.setState({ outcomeSearchText: '' }) } }
                                                            className="clickable fa fa-close form-control-feedback"
                                                            ></span>
                                                    ) : null }
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        ) : null }
                        <div className="row">
                            <div className="col-md-12 ogs-draft-outcomes">
                                <DraftOutcomesTable
                                    { ...this.props }
                                    className="table branded"
                                    outcomes={ sortedOutcomes }
                                    onSelectOutcome={ this.handleAddOutcome.bind(this) }
                                    viewportHeight={ positions ? ((42 * positions.length) + 1) : 400 }
                                    onOutcomeDetails={ (outcome) => { this.setState({ outcomeDetailsId: outcome.id }) } }
                                />
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-md-6">
                                <button className="btn btn-positive btn-block" disabled="disabled" type="button">Export Player Pool</button>
                            </div>
                            <div className="col-md-6">
                                <div className="form-group stacked-inputs">
                                    <label htmlFor="auto_advance">
                                        <input id="auto_advance"
                                            type="checkbox"
                                            checked={ this.state.autoAdvance }
                                            onChange={ (e) => { this.setAutoAdvance(e.target.checked); } }
                                        />
                                        <span>Auto-Advance Position</span>
                                    </label>
                                    <label htmlFor="onlyDisplayStartingPitchers">
                                        <input id="onlyDisplayStartingPitchers" type="checkbox" />
                                        <span>Only Show Probable Pitchers</span>
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="col-md-6">
                        <div className="row">
                            <div className="col-lg-4 col-md-3">
                                <h3 className="table-heading">My Team</h3>
                            </div>
                            <div className="col-lg-8 col-md-9 vspace-10 averageremaining ogs-draft-salary-details">
                                { selections.length === positions.length ? null : (
                                    <p>Avg. Rem. / Player: <span>{ formatSelectionCost(averageRemainingSalary) }</span></p>
                                ) }
                                <p>Salary Cap Remaining: <span>{ formatSelectionCost(remainingSalary) }</span></p>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-md-12 ogs-draft-outcomes">
                                <DraftSelectionsTable
                                    { ...this.props }
                                    className="table branded"
                                    outcomes={ outcomes }
                                    selections={ selections }
                                    positions={ positions }
                                    onOutcomeDetails={ (outcome) => { this.setState({ outcomeDetailsId: outcome.id }) } }
                                    onRemoveSelection={ this.handleRemoveSelection.bind(this) }
                                />
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-md-8">
                                <button className="btn btn-neutral" disabled={ selections.length === 0 } type="button" onClick={ this.handleClearSelections.bind(this) }>Clear</button>
                                { ' ' }
                                <button className="btn btn-positive" disabled="disabled" type="button">Import Lineup</button>
                            </div>
                            <div className="col-md-4 text-right">
                                <button className="btn btn-positive" disabled="disabled" type="button">Reserve Entry</button>
                            </div>
                        </div>
                    </div>
                </div>
                <h1>TODO</h1>
                <ul>
                    <li>Sort by name</li>
                    <li>Sort by team</li>
                    <li>Sort by game</li>
                    <li>Sort by fppg</li>
                    <li>Sort by salary</li>
                    <li>Game details modal</li>
                    <li>Event details modal</li>
                    <li>Challenge friends modal</li>
                    <li>Outcome details modal</li>
                    <li>Ticket edit</li>
                    <li>Lineup create</li>
                    <li>Ticket save</li>
                    <li>Lineup save</li>
                    <li>Selection constraints</li>
                    <li>Ticket constraints</li>
                    <li>Export player pool</li>
                    <li>Import lineup</li>
                    <li>Upsell modal</li>
                    <li>Event description in header</li>
                    <li>Auto-switch on close to Live room</li>
                    <li>Game weather icons</li>
                    <li>Starting lineups</li>
                    <li>Injury statuses</li>
                    <li>Probable pitcher filter</li>
                    <li>Icon legend for starting lineups</li>
                    <li>Opposing pitcher for pitchers</li>
                    <li>Batter/Pitcher Handedness in Outcomes</li>
                    <li>Grouped position tabs</li>
                    <li>RSU logic/checks</li>
                </ul>
            </div>
        );
    }
}

DraftEventPage.propTypes = {
    eventId: React.PropTypes.string.isRequired,
    store: React.PropTypes.object,
    actions: React.PropTypes.object
};
