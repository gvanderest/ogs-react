import React from 'react';
import { Link } from 'react-router-dom';

export default function MyReferralsPage() {
    return (
        <div>
            <h1>My Referrals</h1>
            <Link to="/my-referrals/group-report/">Group Report</Link>
            <h1>TODO</h1>
            <ul>
                <li>Summary stats</li>
                <li>Links to referral groups</li>
                <li>Toolbox / Share / Copy Link button</li>
                <li>Paragraph content</li>
                <li>YouTube embed</li>
                <li>Email share form + submit</li>
                <li>Group report page</li>
            </ul>
        </div>
    )
}
