import React from 'react';


export default class LoginPage extends React.Component {
    login() {
        let username = 'guilt';
        let password = 'password1';

        this.props.actions.customer.authenticateCustomer({ username, password }).then((customer) => {
            this.props.actions.router.push('/contests/');
        });
    }
    render() {
        return (
            <div>
                <h1>Login</h1>
                <button onClick={ this.login.bind(this) } type="button" className="btn btn-positive">Login</button>
            </div>
        )
    }
}
