import React from 'react';
import AccountMenu from '../components/AccountMenu';
import { Link } from 'react-router-dom';


export default class AccountSummaryPage extends React.Component {
    render() {
        return (
            <div>
                <AccountMenu { ...this.props } />
                <h1>Account Summary</h1>
                <Link to="/withdraw">Withdraw Page</Link>
                <h1>TODO</h1>
                <ul>
                    <li>Customer grey panel w/ avatar</li>
                    <li>Play Now link</li>
                    <li>Account Balance section + breakdowns</li>
                    <li>Deposit button</li>
                    <li>Withdraw button</li>
                    <li>Transactions section</li>
                    <li>Transactions pagination</li>
                    <li>Print Button</li>
                    <li>Export transactions CSV</li>
                    <li>experience badge</li>
                </ul>
            </div>
        );
    }
}
