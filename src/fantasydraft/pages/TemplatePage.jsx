import React from 'react';


export default class TemplatePage extends React.PureComponent {
    componentDidMount() {
        let templateId = this.props.templateId;
        console.log('GOING TO LOOK UP ' + templateId);
    }
    render() {
        let templateId = this.props.templateId;
        let template = this.props.store.templates.byId[templateId];
        return (
            <div>
                <h1>Template { templateId }</h1>
                <p>Exists? { template ? 'YES': 'NO' }</p>
            </div>
        );
    }
}
