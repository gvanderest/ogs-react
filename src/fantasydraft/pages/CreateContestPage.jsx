import React from 'react';


export default class EventsPage extends React.Component {
    render() {
        return (
            <div>
                <h1>Create A Contest</h1>
                <h1>TODO</h1>
                <ul>
                    <li>Sport filter</li>
                    <li>Slate picker</li>
                    <li>View included games</li>
                    <li>Contest options filtering</li>
                    <li>Options and combo pickers</li>
                    <li>Go to drafting room afterwards for tickets</li>
                    <li>H2H filtering mode</li>
                    <li>Private contest naming</li>
                    <li>League filter dropdowns</li>
                </ul>
            </div>
        );
    }
}
