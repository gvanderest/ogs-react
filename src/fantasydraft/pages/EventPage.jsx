import React from 'react';
import { Link } from 'react-router-dom';
import translate from '../../utils/translate';
import DraftEventPage from './DraftEventPage';
import WatchEventPage from './WatchEventPage';

import EventHeader from '../components/EventHeader';

import formatTicketCost from '../../utils/formatTicketCost';
import formatEventPayout from '../../utils/formatEventPayout';

import Event from '../../models/Event';


export default class EventPage extends React.Component {
    componentDidMount() {
        this.props.actions.events.fetchEventDetails(this.props.eventId).then(null, () => {
            this.props.actions.router.push('/');
        });
    }
    render(){
        let rawEvent = this.props.store.events.byId[this.props.eventId];
        let event = rawEvent ? new Event(rawEvent) : null;
        let ticketId = this.props.ticketId;
        let ticket = ticketId ? this.props.store.tickets.byId[ticketId] : null;

        if (!event) {
            return (
                <div>
                    <h1>Not found yet</h1>
                </div>
            );
        }
        return (
            <div>
                <EventHeader event={ event } />

                { event ? (
                    event.status === 'o' ? (
                        <DraftEventPage { ...this.props } eventId={ event.id } />
                    ) : (
                        <WatchEventPage { ...this.props } eventId={ event.id } />
                    )
                ) : null }
            </div>
        );
    }
}

EventPage.propTypes = {
    eventId: React.PropTypes.string.isRequired,

    store: React.PropTypes.shape({
        events: React.PropTypes.shape({
            byId: React.PropTypes.object
        })
    }),

    actions: React.PropTypes.shape({
        events: React.PropTypes.shape({
            fetchEventDetails: React.PropTypes.func.isRequired,
        }),
        router: React.PropTypes.shape({
            push: React.PropTypes.func.isRequired
        })
    })
};
