import React from 'react';
import AccountMenu from '../components/AccountMenu';


export default class WithdrawPage extends React.Component {
    render() {
        return (
            <div>
                <AccountMenu { ...this.props } />
                <h1>Withdraw</h1>
                <h1>TODO</h1>
                <ul>
                    <li>Pre-filled form items</li>
                    <li>Display of amounts</li>
                    <li>Validation</li>
                    <li>Submission</li>
                    <li>Error handling</li>
                    <li>Content</li>
                </ul>
            </div>
        );
    }
}
