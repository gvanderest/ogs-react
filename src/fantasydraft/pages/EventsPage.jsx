import translate from '../../utils/translate';
import { Link } from 'react-router-dom';
import React from 'react';
import formatCountdown from '../../utils/formatCountdown';
import formatTicketCost from '../../utils/formatTicketCost';
import formatEventPayout from '../../utils/formatEventPayout';
import ContextIcon from '../components/ContextIcon';
import InfiniteScroll from '../../components/InfiniteScroll';
import Event from '../../models/Event';


class EventsTable extends InfiniteScroll {
    tableDidMount() {
        let self = this;
        this.countdownInterval = setInterval(() => {
            self.forceUpdate()
        }, 1000);
    }
    tableWillUnmount() {
        clearInterval(this.countdownInterval);
    }
    getRecords() {
        return this.props.events;
    }
    renderHeader() {
        return (
            <thead>
                <tr>
                    <th></th>
                    <th className="text-left">Contests</th>
                    <th></th>
                    <th></th>
                    <th>Entry Fee</th>
                    <th>Prize Pool</th>
                    <th>Entries</th>
                    <th>Live In</th>
                    <th></th>
                </tr>
            </thead>
        );
    }
    renderRecords(records) {
        return records.map((event) => {
            return (
                <tr key={ event.id }>
                    <td className="xsmall-width"><ContextIcon context={ event.context } /></td>
                    <td className="large-width text-left no-border-right"><a>{ event.description }</a></td>
                    <td className="icon-width no-border-left no-border-right">{ event.isMultiEntry() ? '[M]' : '' }</td>
                    <td className="icon-width no-border-left">{ event.isGuaranteed() ? '[G]' : '' }</td>
                    <td className="default-width">{ formatTicketCost(event.ticketCost) }</td>
                    <td className="default-width">{ formatEventPayout(event.payout) }</td>
                    <td className="default-width">{ event.ticketCount } / { event.ticketMax }</td>
                    <td className="default-width">{ formatCountdown(event.closeMoment) }</td>
                    <td className="btn-width"><Link className="btn btn-positive btn-block" to={ `/contest/${ event.id }/` }>Enter</Link></td>
                </tr>
            );
        });
    }
}


export default class EventsPage extends React.Component {
    componentDidMount() {
        console.log('MURPMURP');
    }
    render() {
        let events = Object.values(this.props.store.events.byId).map((event) => {
            return new Event(event);
        });

        return (
            <div>
                <div className="row hidden-sm hidden-xs button-filters">
                    <div className="col-md-10">
                        <div className="pull-left">
                            <div className="sport-select-filter buttons table-filter clearfix">
                                <div className="btn btn-branded">ALL SPORTS</div>
                                <div className="btn btn-branded"><ContextIcon context="MLB" /> MLB</div>
                                <div className="btn btn-branded"><ContextIcon context="PGA" /> PGA</div>
                                <div className="btn btn-branded"><ContextIcon context="NBA" /> NBA</div>
                                <div className="btn btn-branded"><ContextIcon context="NHL" /> NHL</div>
                                <div className="btn btn-branded btn-disabled"><ContextIcon context="NFL" /> NFL</div>
                            </div>
                        </div>
                        <div className="form-group lobby-search with-buttons pull-left inline-button visible-lg">
                            <input className="form-control placeholder-visible" placeholder="Search Contests" type="text" />
                        </div>
                    </div>
                    <div className="col-md-2">
                        <Link className="btn pull-right btn-positive create-a-contest" to="/create-a-contest/">Create A Contest</Link>
                    </div>
                </div>
                <div className="row hidden-sm hidden-xs button-filters">
                    <div className="col-md-10">
                        <div className="sport-select-filter buttons table-filter clearfix">
                            <div className="btn btn-branded">ALL TYPES</div>
                            <div className="btn btn-branded">GUARANTEED</div>
                            <div className="btn btn-branded">LEAGUES</div>
                            <div className="btn btn-branded">50/50s</div>
                            <div className="btn btn-branded">MULTIPLIERS</div>
                            <div className="btn btn-branded">HEAD-TO-HEAD</div>
                            <div className="btn btn-branded">QUALIFIERS</div>
                        </div>
                    </div>
                    <div className="col-md-2 text-right">
                        <a className="btn btn-branded btn-branded-link advanced-filters">ADVANCED FILTERS</a>
                    </div>
                </div>
                <EventsTable
                    { ...this.props }
                    className="table branded"
                    events={ events }
                    viewportHeight={ 500 }
                />
                <div className="row">
                    <div className="col-md-12 icon-legend-holder clearfix">
                        <div className="icon-legend text-center">
                            <span><i className="experience-icon event beginner"></i> = Beginner</span>
                            <span><i className="experience-icon event intermediate"></i> = Intermediate</span>
                            <span><i className="fa fa-star event-type-icon multientry"></i> = Multi Entry</span>
                            <span><i className="fa fa-star event-type-icon guaranteed"></i> = Guaranteed to Run</span>
                        </div>
                    </div>
                </div>
                <h1>TODO</h1>
                <ul>
                    <li>Live ticket counts</li>
                    <li>Sorting by fields</li>
                    <li>Default sort</li>
                    <li>Blue button on refresh</li>
                    <li>Basic filtering</li>
                    <li>Advanced filtering</li>
                    <li>Tracking of filled contests</li>
                    <li>Enter Again button</li>
                    <li>MLE icon</li>
                    <li>Tooltips</li>
                    <li>Event details modal</li>
                    <li>"Next Start" logic for filtering</li>
                    <li>Banner Ad Positions</li>
                    <li>Search textbox</li>
                    <li>Tooltips</li>
                    <li>Mobile formatting</li>
                    <li>Icon legend</li>
                </ul>
                <h1>FANTACYACES IMPORT PAGE</h1>
                <ul>
                    <li>Anything</li>
                </ul>
                <h1>LANDING PAGE TODO</h1>
                <ul>
                    <li>Blog integration</li>
                </ul>
                <h1>TODO MENU</h1>
                <ul>
                    <li>Login modal for real</li>
                    <li>Register modal for real</li>
                    <li>Forgot password modal for real</li>
                    <li>Reset password page for real</li>
                </ul>
                <h1>GENERAL FUNCTIONALITY/MODALS</h1>
                <ul>
                    <li>Create a contest</li>
                    <li>Invite friends to contests</li>
                </ul>
            </div>
        );
    }
}
