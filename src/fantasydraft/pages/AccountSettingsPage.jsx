import React from 'react';
import AccountMenu from '../components/AccountMenu';


export default class AccountSettingsPage extends React.Component {
    render() {
        return (
            <div>
                <AccountMenu { ...this.props } />
                <h1>Account Settings</h1>
                <h1>TODO</h1>
                <ul>
                    <li>Basic Info section</li>
                    <li>Saved Payment Options</li>
                    <li>My Contests Settings</li>
                    <li>Password Change</li>
                    <li>Email & Sharing Settings</li>
                </ul>
            </div>
        )
    }
}
