import React from 'react';
import EventPage from './EventPage';


export default class TicketPage extends React.Component {
    componentDidMount() {
        this.props.actions.tickets.fetchTicketDetails(this.props.ticketId).then(null, () => {
            console.log('TICKET NOT FOUND');
        });
    }
    render() {
        let ticketId = this.props.ticketId;
        let ticket = this.props.store.tickets.byId[ticketId];
        return (
            <div>
                <h1>Ticket { ticketId } { ticket ? 'TICKET' : 'Loading' }</h1>
                { ticket ? (
                    <EventPage { ...this.props } eventId={ ticket.eventId }></EventPage>
                ) : (
                    <p>Loading..</p>
                ) }
            </div>
        );
    }
}

TicketPage.propTypes = {
    ticketId: React.PropTypes.string.isRequired
};
