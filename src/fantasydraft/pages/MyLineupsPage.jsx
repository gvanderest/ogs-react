import React from 'react';
import _ from 'lodash';
import { CONTEXTS } from '../../settings';

import Template from '../../models/Template';
import ContextFilters from '../components/ContextFilters';
import OutcomeDetailsModal from '../components/OutcomeDetailsModal';
import TemplateLineup from '../components/TemplateLineup';


const TEMPLATE_STATUS_FILTERS = [
    { sortOrder: 0, id: 'all', predicate: () => { return true; }, label: 'All Lineups' },
    { sortOrder: 1, id: 'not-entered', predicate: (template) => { return template.tickets.length === 0 }, label: 'Not Entered' },
    { sortOrder: 2, id: 'upcoming', predicate: (template) => { return template.getEventGames().isOpen() }, label: 'Upcoming' },
    { sortOrder: 3, id: 'live', predicate: (template) => { return template.getEventGames().isClosed() }, label: 'Live' },
    { sortOrder: 4, id: 'completed', predicate: (template) => { return template.getEventGames().isFinalized() }, label: 'Completed' },
];


export default class MyLineupsPage extends React.PureComponent {
    constructor() {
        super();
        this.state = {
            outcomeDetailsId: null,
            selectedContext: null,
            statusFilter: TEMPLATE_STATUS_FILTERS[0].id
        }
    }
    componentDidMount() {
        let self = this;
        this.timerId = setInterval(() => {
            self.forceUpdate();
        }, 1000);
    }
    componentWillUnmount() {
        clearInterval(this.timerId);
    }
    componentWillReceiveProps(nextProps) {
        // Find the first context available to select as filter when templates load
        let templates = Object.values(nextProps.store.templates.byId);
        let contextsAvailable = _.map(templates, 'context');
        if (!this.state.selectedContext) {
            let firstAvailableContext = _.find(CONTEXTS, (context) => {
                return _.includes(contextsAvailable, context.context);
            })
            if (firstAvailableContext) {
                this.setState({
                    selectedContext: firstAvailableContext.context
                });
            }
        }
    }
    confirmDelete(template) {
        if (window.confirm('Are you sure?')) {
            this.props.actions.templates.deleteTemplate(template.id).then(() => {
            }, () => {
                window.alert('Error while deleting template');
            });
        }
    }
    render() {
        let templates = Object.values(this.props.store.templates.byId);

        let filteredTemplates = _.filter(templates, (template) => {

            if (template.context !== this.state.selectedContext) {
                return false;
            }

            if (this.state.statusFilter) {
                let statusFilter = _.find(TEMPLATE_STATUS_FILTERS, { id: this.state.statusFilter });
                if (!statusFilter.predicate(new Template(template))) {
                    return false;
                }
            }

            return true;
        });
        let sortedTemplates = filteredTemplates;

        let maxSelections = 0;
        sortedTemplates.forEach((template) => {
            maxSelections = Math.max(maxSelections, template.selections.length);
        });

        let self = this;
        return (
            <div className="my-lineups">
                <div className="row">
                    <div className="col-md-9">
                        <div className="row">
                            <div className="col-md-12">
                                <ContextFilters
                                    selectedContext={ this.state.selectedContext }
                                    items={ templates }
                                    onSelectContext={ (context) => { this.setState({ selectedContext: context.context }) } }
                                />
                                <div className="sport-template-filters">
                                    <div className="row">
                                        { TEMPLATE_STATUS_FILTERS.map((filter) => {
                                            let buttonClasses = ['btn', 'btn-branded', 'btn-block'];
                                            if (filter.id === self.state.statusFilter) {
                                                buttonClasses.push('selected');
                                            }
                                            return (
                                                <div key={ filter.id } className="col-md-2 sport-button">
                                                    <a
                                                        onClick={ () => { self.setState({ statusFilter: filter.id }) } }
                                                        className={ buttonClasses.join(' ') }>{ filter.label }</a>
                                                </div>
                                            );
                                        }) }
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="col-md-3">
                        <div className="row">
                            <div className="col-md-12">
                                <p><button className="btn btn-positive btn-block" type="button">Create New Lineup</button></p>
                                <p><button className="btn btn-positive btn-block" type="button">Upload Lineups</button></p>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="row my-lineups">
                    { this.state.outcomeDetailsId ? (
                        <OutcomeDetailsModal
                            { ...this.props }
                            onClose={ () => { this.setState({ outcomeDetailsId: null }) } }
                            outcomeId={ this.state.outcomeDetailsId }
                        />
                    ) : null }
                    <div className="col-md-12 roster-holder">
                        { sortedTemplates.map((template) => {
                            return (
                                <TemplateLineup
                                    { ...self.props }
                                    template={ template }
                                    key={ template.id }
                                    maxSelections={ maxSelections }
                                    onOutcomeDetails={ (outcome) => { self.setState({ outcomeDetailsId: outcome.id }) } }
                                    onDelete={ self.confirmDelete.bind(self, template) }
                                />
                            );
                        }) }
                    </div>
                    <h1>TODO</h1>
                    <ul>
                        <li>Export button</li>
                        <li>Entries button</li>
                        <li>Delete button</li>
                        <li>Injury status</li>
                        <li>Starting pitchers/etc.</li>
                        <li>Create new lineup</li>
                        <li>Upload lineups</li>
                        <li>Points tooltips</li>
                    </ul>
                </div>
            </div>
        );
    }
}


MyLineupsPage.propTypes = {
    actions: React.PropTypes.shape({
        templates: React.PropTypes.object.isRequired
    }),
    store: React.PropTypes.shape({
        templates: React.PropTypes.object.isRequired,
        eventGames: React.PropTypes.object.isRequired,
    })
};
