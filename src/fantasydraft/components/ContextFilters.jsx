import React from 'react';
import { CONTEXTS } from '../../settings';


export default class ContextFilters extends React.PureComponent {
    render() {
        // items can be provided with a 'context' node to be counted and used for disabling
        let items = this.props.items;
        let itemContextNode = this.props.itemContextNode || 'context';
        let contextCounts = _.countBy(items, itemContextNode);

        return (
            <div className="sport-template-filters">
                <div className="row">
                    { CONTEXTS.map((context) => {
                        let itemCount = contextCounts[context.context] || 0;

                        let buttonClasses = ['btn', 'btn-block', 'btn-branded'];
                        if (context.context === this.props.selectedContext) {
                            buttonClasses.push('selected');
                        }
                        if (items && !itemCount) {
                            buttonClasses.push('btn-disabled');
                        }

                        return (
                            <div key={ context.id } className={ 'col-md-2 sport-button' }>
                                <a
                                    className={ buttonClasses.join(' ') }
                                    onClick={ this.props.onSelectContext.bind(this, context) }
                                >
                                    <i className={ context.iconClasses }></i>
                                    { ' ' }
                                    <span>{ context.label }{ items ? (` (${ itemCount })`) : '' }</span>
                                </a>
                            </div>
                        );
                    }) }
                </div>
            </div>
        );
    }
}
