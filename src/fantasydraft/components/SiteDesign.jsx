import { Link } from 'react-router-dom';
import React from 'react';
import formatOgsDollar from '../../utils/formatOgsDollar';


export default class SiteDesign extends React.Component {
    constructor() {
        super();
        this.state = {
            accountMenuOpen: false
        };
    }
    getMenuItemClassString(url) {
        let classes = [];
        if (location.pathname === url) {
            classes.push('selected');
        }
        return classes.join(' ');
    }
    accountMenuIsOpen() {
        return this.state.accountMenuOpen;
    }
    toggleAccountMenu() {
        this.setState({
            accountMenuOpen: !this.state.accountMenuOpen
        });
    }
    hideAccountMenu() {
        this.setState({
            accountMenuOpen: false
        });
    }
    splashPageIsBeingDisplayed() {
        return location.pathname === '/';
    }
    render() {
        let fetchingCustomer = this.props.store.customer.fetchingActiveCustomer;
        let customer = this.props.store.customer.activeCustomer;
        return (
            <div className="shim" onClick={ () => { this.hideAccountMenu() } } >
                <div className={ 'wrapper-full nav-wrapper ' + (this.splashPageIsBeingDisplayed() ? 'no-background' : '') }>
                    <div className="container">
                        <div className="navbar navbar-button-bar" role="navigation">
                            <div className="col-md-2">
                                <Link className="navbar-brand" to={ customer ? '/contests/' : '/' }>
                                    <img className="logo" src={ '/' + require('../images/white-logo.png') } alt="FantasyDraft.com" />
                                </Link>
                            </div>
                            <div className="col-md-10 clearfix">
                                <div className="row">
                                    <div className="collapse navbar-collapse">
                                        <div className="col-md-8 col-sm-8 clearfix">
                                            <ul className="nav navbar-nav navbar-center">
                                                <li><Link className={ this.getMenuItemClassString('/contests/') } to="/contests/">Contest Lobby</Link></li>
                                                <li><Link className={ this.getMenuItemClassString('/my-contests/') } to="/my-contests/">My Contests</Link></li>
                                                <li><Link className={ this.getMenuItemClassString('/my-lineups/') } to="/my-lineups/">My Lineups</Link></li>
                                                { customer ? (
                                                    <li><Link className={ this.getMenuItemClassString('/my-referrals/') } to="/my-referrals/">My Referrals</Link></li>
                                                ) : (
                                                    <li><Link className={ this.getMenuItemClassString('/referrals/') } to="/referrals/">Referrals</Link></li>
                                                ) }
                                            </ul>
                                        </div>
                                        <div className="col-md-4 col-sm-4 clearfix navbar-block-holder">
                                            <ul className="nav navbar-nav navbar-block pull-right">
                                                { (fetchingCustomer && !customer) ? (
                                                    <div className="col-md-3 clearfix navbar-block-holder">
                                                        <div className="requesting-customer desktop">
                                                            <div className="loading">
                                                                <h3 className="text-center">
                                                                    <i className="fa fa-spinner fa-3x fa-pulse"></i>
                                                                </h3>
                                                            </div>
                                                        </div>
                                                    </div>
                                                ) : (
                                                    customer ? (
                                                        [
                                                            <li key="deposit"><Link className="btn-positive" to="/deposit/">Deposit</Link></li>,
                                                            <li key="dropdown" className={ 'dropdown with-deposit ' + (this.accountMenuIsOpen() ? 'open' : '') }>
                                                                <a onClick={ (e) => { e.stopPropagation(); this.toggleAccountMenu() } } className="dropdown-toggle blocked-account-balance hidden-sm hidden-xs scroll-to-top-override">
                                                                    <div className="user-info">
                                                                        <div className="username no-wrap">{ customer.user.username } [EXP]</div>
                                                                        <div className="user-balance">{ formatOgsDollar(customer.account.balance) }</div>
                                                                    </div>
                                                                    <div className="caret-holder">
                                                                        <i className="fa fa-chevron-down"></i>
                                                                    </div>
                                                                    <div className="user-image">
                                                                        <img className="img-responsive pull-left avatar" src={ customer.settings.avatar ? `https://dnckudql83kkc.cloudfront.net/${ customer.settings.avatar }` : ('/' + require('../images/no-avatar.png')) } />
                                                                    </div>
                                                                </a>
                                                                <ul onClick={ () => { this.hideAccountMenu() } } className="dropdown-menu full-width-dropdown" role="menu">
                                                                    <li><Link to="/profile/account/">Account</Link></li>
                                                                    <li><Link to="/profile/settings/">Settings</Link></li>
                                                                    <li><Link to="/deposit/">Add Funds</Link></li>
                                                                    <li><a href="https://lockerroom.fantasydraft.com/store/" target="_blank">Shop FD Gear</a></li>
                                                                    <li><a href="https://lockerroom.fantasydraft.com/" target="_blank">Locker Room</a></li>
                                                                    <li><Link to="/help-center/">Help Center</Link></li>
                                                                    <li><a onClick={ () => { this.props.actions.customer.logoutCustomer() } }>Logout</a></li>
                                                                </ul>
                                                            </li>
                                                        ]
                                                    ) : (
                                                        [
                                                            <li key="register"><Link className="btn-positive" to="/register/">Sign Up</Link></li>,
                                                            <li key="login"><Link className="btn-positive" to="/login/">Login</Link></li>
                                                        ]
                                                    )
                                                ) }
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                { this.splashPageIsBeingDisplayed() ? (
                    this.props.children
                ) : (
                    <div className="container">
                        <div className="content-wrapper">
                            { this.props.children }
                        </div>
                    </div>
                ) }
                <div className={ 'wrapper-full footer ' + (this.splashPageIsBeingDisplayed() ? 'no-background' : '') }>
                    <div className="container">
                        <div className="col-md-3">
                            <div className="row">
                                <div className="bordered clearfix">
                                    <div className="col-md-12">
                                        <Link to={ customer ? '/contests/' : '/' }>
                                            <img className="img-responsive logo" src={ '/' + require('../images/white-logo.png') } alt="FantasyDraft.com" />
                                        </Link>
                                    </div>
                                    <div className="col-md-12">
                                        <div className="nav-social">
                                            <a href="https://www.facebook.com/FantasyDraft" target="_blank"><i className="fa fa-facebook"></i></a>
                                            { ' ' }
                                            <a href="https://www.twitter.com/FantasyDraft" target="_blank"><i className="fa fa-twitter"></i></a>
                                            { ' ' }
                                            <a href="https://instagram.com/FantasyDraft" target="_blank"><i className="fa fa-instagram"></i></a>
                                        </div>
                                    </div>
                                    <div className="col-md-12">
                                        <p className="text-center copyright">&copy; 2017-2017 FantasyDraft, LLC.<br />All Rights Reserved.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="col-md-7">
                            <div className="row">
                                <div className="col-md-4">
                                    <ul className="list-group links lrg-links">
                                        <li className="list-group-item"><Link to="/contests/">Contest Lobby</Link></li>
                                        <li className="list-group-item"><Link to="/my-contests/">My Contests</Link></li>
                                        <li className="list-group-item"><Link to="/my-lineups/">My Lineups</Link></li>
                                        <li className="list-group-item"><Link to="/deposit/">Deposit</Link></li>
                                        <li className="list-group-item"><a href="https://lockerroom.fantasydraft.com/store/" target="_blank">Shop FD Gear</a></li>
                                        <li className="list-group-item"><a href="https://lockerroom.fantasydraft.com/" target="_blank">Locker Room</a></li>
                                        <li className="list-group-item"><a href="https://page.fantasydraft.com/responsible-gaming/" target="_blank">Responsible Gaming</a></li>
                                        <li className="list-group-item"><a href="https://page.fantasydraft.com/fantasydraft-101/" target="_blank">FantasyDraft 101</a></li>
                                    </ul>
                                </div>
                                <div className="col-md-4">
                                    <ul className="list-group links">
                                        <li className="list-group-item"><Link to="/referrals/">Referrals</Link></li>
                                        <li className="list-group-item"><Link to="/my-referrals/">My Referrals</Link></li>
                                    </ul>
                                    <ul className="list-group links">
                                        <li className="list-group-item"><Link to="/help-center/fantasydraft-basics/">Help Center</Link></li>
                                        <li className="list-group-item"><Link to="/help-center/contest-types/">Contest Types</Link></li>
                                        <li className="list-group-item"><Link to="/help-center/scoring-and-rules/">Scoring &amp; Rules</Link></li>
                                        <li className="list-group-item"><Link to="/help-center/faqs/">Frequent Questions</Link></li>
                                        <li className="list-group-item"><Link to="/help-center/support/">Support</Link></li>
                                    </ul>
                                </div>
                                <div className="col-md-4">
                                    <ul className="list-group links">
                                        <li><Link to="/about/">About FantasyDraft</Link></li>
                                        <li><Link to="/about/press/">Press Box</Link></li>
                                        <li><Link to="/about/terms-of-use/">Terms of Use</Link></li>
                                        <li><Link to="/about/privacy-policy/">Privacy Policy</Link></li>
                                        <li><a href="https://lockerroom.fantasydraft.com/careers" target="_blank">Careers</a></li>
                                    </ul>
                                    <ul className="list-group links">
                                        <li><Link to="/profile/account/">My Profile</Link></li>
                                        <li><Link to="/profile/account/">Account</Link></li>
                                        <li><Link to="/profile/settings/">Settings</Link></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div className="col-md-2">
                            <div className="row">
                                <div className="bordered clearfix">
                                    <img className="img-responsive" src={ '/' + require('../images/affiliated-logos.png') } alt="" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

SiteDesign.propTypes = {
    children: React.PropTypes.array,
    actions: React.PropTypes.shape({
        customer: React.PropTypes.shape({
            loginCustomer: React.PropTypes.func,
            logoutCustomer: React.PropTypes.func,
        }),
        router: React.PropTypes.shape({
            push: React.PropTypes.func
        })
    }),
    store: React.PropTypes.shape({
        customer: React.PropTypes.shape({
            activeCustomer: React.PropTypes.object,
            fetchingActiveCustomer: React.PropTypes.bool
        }),
        events: React.PropTypes.shape({
            byId: React.PropTypes.object.isRequired
        })
    })
};
