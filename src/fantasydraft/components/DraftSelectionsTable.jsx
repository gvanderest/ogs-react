import React from 'react';
import DraftOutcome from './DraftOutcome';


export default class DraftSelectionsTable extends React.Component {
    render() {
        let self = this;
        return (
            <table className="table branded">
                <thead>
                    <tr>
                        <th>POS</th>
                        <th></th>
                        <th className="text-left">Player</th>
                        <th>Team</th>
                        <th>Game</th>
                        <th>FPPG</th>
                        <th>Salary</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    { this.props.positions ? (
                        this.props.positions.map((position) => {
                            let selection = _.find(self.props.selections, { positionId: position.id });
                            let outcome = selection ? _.find(self.props.outcomes, { id: selection.outcomeId }) : null;
                            return <DraftOutcome
                                key={ position ? position.id : outcome.id }
                                outcome={ outcome }
                                position={ position }
                                selection={ selection }
                                onRemoveSelection={ self.props.onRemoveSelection }
                                onSelectOutcome={ self.props.onSelectOutcome }
                                onOutcomeDetails={ self.props.onOutcomeDetails }
                                />
                        })
                    ) : (
                        <tr>
                            <td colSpan={ 10 }>No records.</td>
                        </tr>
                    ) }
                </tbody>
            </table>
        );
    }
    renderRecord(position) {
        return super.renderRecord(outcome, position, selection);
    }
}
