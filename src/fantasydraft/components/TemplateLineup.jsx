import React from 'react';
import ContextIcon from '../components/ContextIcon';
import formatSelectionCost from '../../utils/formatSelectionCost';
import formatPoints from '../../utils/formatPoints';
import formatDate from '../../utils/formatDate';
import { Link } from 'react-router-dom';
import Selection from '../../models/Selection';
import formatCountdown from '../../utils/formatCountdown';
import Template from '../../models/Template';
import EventGames from '../../models/EventGames';
import _ from 'lodash';


export default class TemplateLineup extends React.Component {
    render() {
        let self = this;
        let template = this.props.template;
        let selections = _.orderBy(template.selections, 'eventPosition.sortOrder');

        let eventGames = this.props.store.eventGames.byId[template.externalId];
        if (eventGames) { eventGames = new EventGames(eventGames); }

        let summedPoints = _.sum(_.map(selections, 'pointsEarned'));
        let summedPMR = _.sum(_.map(selections, 'outcome.eventScoringTimeIndicator'));

        let pmrLabel = {
            'PGA': 'PHR',
            'MLB': 'PIR',
        }[eventGames.context] || 'PMR';

        return (
            <div key={ template.id } className={ 'col-md-4 template ' }>
                <div className={ 'roster clearfix vspace-20-inverse ' + (eventGames.isOpen() ? (template.tickets.length ? 'upcoming ': 'new ') : '') }>
                    <div className="table-topper roster-topper clearfix hidden-sm hidden-xs">
                        { (eventGames.isOpen() && template.tickets.length === 0) ? (
                            <div
                                className="delete-ticket-template-icon"
                                onClick={ this.props.onDelete.bind(this, template) }
                            ><i className="fa fa-remove"></i></div>
                        ) : null }
                        <div className="row small-gutter">
                            <div className="col-md-4">
                                <h1><ContextIcon invert={ true } context={ eventGames.context } />&nbsp;{ eventGames.context }</h1>
                                { !eventGames.isOpen() ? (
                                    <h2><strong>Fantasy Points:</strong> <span>{ formatPoints(summedPoints) }</span></h2>
                                ) : null }
                            </div>
                            <div className="col-md-8">
                                <table className={ 'table branded headless pull-right '}>
                                    <tbody>
                                        <tr>
                                            <td className="roster-status">
                                                { eventGames.isOpen() ? (
                                                    template.tickets.length === 0 ? (
                                                        <h2><strong>NOT ENTERED</strong></h2>
                                                    ) : (
                                                        <h2><strong>UPCOMING</strong></h2>
                                                    )
                                                ) : (
                                                    eventGames.isClosed() ? (
                                                        <h2><strong>LIVE</strong></h2>
                                                    ) : (
                                                        <h2><strong>COMPLETED</strong></h2>
                                                    )
                                                ) }
                                            </td>
                                            <td>
                                                { eventGames.isOpen() ? (
                                                    <h2 className="event-games-countdown">{ formatCountdown(eventGames.closeMoment) }</h2>
                                                ) : (
                                                    <h2 className="event-games-countdown">{ `${ summedPMR } ${ pmrLabel }` }</h2>
                                                ) }
                                            </td>
                                        </tr>
                                        <tr>
                                            <td><h2><strong>Entries:</strong></h2></td>
                                            <td><h2>{ template.tickets.length }</h2></td>
                                        </tr>
                                        <tr>
                                            <td><h2><strong>Last Edit:</strong></h2></td>
                                            <td><h2>{ formatDate(template.modifiedMoment) }</h2></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div className="table-content">
                        <table className="table branded ogs-draft-roster">
                            <thead>
                                <tr>
                                    { eventGames.context !== 'PGA' ? (
                                        <th>POS</th>
                                    ) : null }
                                    <th className="text-left">Player</th>
                                    <th>Salary</th>
                                    { eventGames.isOpen() ? (
                                        <th>FPPG</th>
                                    ) : (
                                        <th>FP</th>
                                    ) }
                                </tr>
                            </thead>
                            <tbody>
                                { selections.map((selection) => {
                                    selection = new Selection(selection);
                                    return (
                                        <tr key={ selection.id } className={ selection.scoreIsDropped() ? 'scoring-dropped' : '' }>
                                            { eventGames.context !== 'PGA' ? (
                                                <td className="highlight">{ selection.eventPosition.name }</td>
                                            ) : null }
                                            <td className="text-left">
                                                <a onClick={ () => { self.props.onOutcomeDetails(selection.outcome) } }>{ selection.outcome.name }</a>
                                            </td>
                                            <td>{ formatSelectionCost(selection.outcome.selectionCost) }</td>
                                            { eventGames.isOpen() ? (
                                                <td>
                                                    <span className="points-earned">{ formatPoints(selection.outcome.plannedPoints) }</span>
                                                </td>
                                            ) : (
                                                <td>
                                                    <span className="points-earned">{ formatPoints(selection.outcome.pointsAvailable) }</span>
                                                </td>
                                            ) }
                                        </tr>
                                    );
                                }) }
                                { (() => {
                                    if (!this.props.maxSelections) {
                                        return null;
                                    }
                                    let fillerRows = [];
                                    let rowsToFill = Math.max(0, this.props.maxSelections - selections.length);
                                    for (let x = 0; x < rowsToFill; x++) {
                                        fillerRows.push(
                                            <tr key={ `filler-${ x }` }>
                                                <td colSpan="3"></td>
                                            </tr>
                                        )
                                    }
                                    return fillerRows;
                                })() }
                            </tbody>
                        </table>
                    </div>
                    <div className="table-footer roster-footer clearfix">
                        <div className="row">
                            <div className="col-md-12 text-center">
                                <Link className="btn btn-positive" disabled={ eventGames.isOpen() ? '' : 'disabled' } to={ `/template/${ template.id }/` }>Edit</Link>
                                { ' ' }
                                <a className="btn btn-positive" disabled={ eventGames.isOpen() ? '' : 'disabled' }>Export</a>
                                { ' ' }
                                <a className="btn btn-positive">Entries</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}
