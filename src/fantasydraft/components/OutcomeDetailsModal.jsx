import React from 'react';
import Modal, { ModalTitle, ModalFooter, ModalBody } from './Modal';


export default class OutcomeDetailsModal extends React.Component {
    render() {
        return (
            <Modal onClose={ this.props.onClose.bind(this) }>
                <ModalTitle>Outcome Details</ModalTitle>
                <ModalBody>
                    <p>Information for outcome #{ this.props.outcomeId }</p>
                </ModalBody>
            </Modal>
        );
    }
}
