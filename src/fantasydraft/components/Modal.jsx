import React from 'react';


export class ModalHeader extends React.Component {
    render() {
        return (
            <div className="modal-header">
                { this.props.children }
            </div>
        );
    }
}

export class ModalTitle extends React.Component {
    render() {
        return(
            <ModalHeader>
                <h2>{ this.props.children }</h2>
            </ModalHeader>
        );
    }
}

export class ModalBody extends React.Component {
    render() {
        return (
            <div className="modal-body">
                { this.props.children }
            </div>
        );
    }
}

export class ModalFooter extends React.Component {
    render() {
        return (
            <div className="modal-footer">
                { this.props.children }
            </div>
        );
    }
}

export default class Modal extends React.Component {
    handleShadowClose(e) {
        if (this.props.disableShadowClose) {
            return;
        }
        this.handleClose(e);
    }
    handleClose(e) {
        if (this.props.onClose) {
            this.props.onClose(e);
        }
    }
    render() {
        return (
            <div className="modal" style={ { display: 'block' } } onClick={ this.handleShadowClose.bind(this) }>
                <div className="modal-dialog modal-dialog-animate modal-sm" style={ { marginTop: '100px' } } onClick={ (e) => { e.stopPropagation() } }>
                    <div className="modal-content">
                        { this.props.onClose ? (
                            <button className="close" type="button" onClick={ this.handleClose.bind(this) }>
                                <i className="fa fa-times"></i><span className="sr-only">Close</span>
                            </button>
                        ) : null }
                        <div className="clearfix">
                            { this.props.children }
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}
