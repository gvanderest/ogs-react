import React from 'react';
import { Link } from 'react-router-dom';


export default class AccountMenu extends React.Component {
    getLinkClasses(url) {
        console.log(this.props.store.router.location.pathname);
        if (this.props.store.router.location.pathname === url) {
            return 'selected';
        }
        return '';
    }
    render() {
        return (
            <nav className="navbar subnavbar" role="navigation">
                <div className="container">
                    <div className="row">
                        <div className="col-md-10 col-md-offset-2 clearfix">
                            <div className="row">
                                <div className="collapse navbar-collapse">
                                    <div className="row">
                                        <div className="col-md-9 clearfix">
                                            <ul className="nav navbar-nav navbar-center">
                                                <li>
                                                    <Link className={ this.getLinkClasses('/profile/account/') } to="/profile/account/">ACCOUNT</Link>
                                                </li>
                                                <li>
                                                    <Link className={ this.getLinkClasses('/profile/settings/') } to="/profile/settings/">SETTINGS</Link>
                                                </li>
                                                <li><Link to="/deposit/">Add Funds</Link></li>
                                                <li>
                                                    <Link className={ this.getLinkClasses('/profile/limits/') } to="/profile/limits/">LIMITS</Link>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </nav>
        );
    }
}
