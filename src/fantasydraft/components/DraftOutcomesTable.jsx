import React from 'react';
import DraftOutcome from './DraftOutcome';
import InfiniteScroll from '../../components/InfiniteScroll';


export default class DraftOutcomesTable extends InfiniteScroll {
    getRecords() {
        return this.props.outcomes;
    }
    renderHeader() {
        return (
            <thead>
                <tr>
                    <th>POS</th>
                    <th className="text-left">Player</th>
                    <th>Team</th>
                    <th>Game</th>
                    <th>FPPG</th>
                    <th>Salary</th>
                    <th></th>
                </tr>
            </thead>
        );
    }
    renderRecord(outcome) {
        return (
            <DraftOutcome
                key={ outcome.id }
                outcome={ outcome }
                onRemoveSelection={ this.props.onRemoveSelection }
                onOutcomeDetails={ this.props.onOutcomeDetails }
                onSelectOutcome={ this.props.onSelectOutcome }
                />
        );
    }
}
