import React from 'react';
import formatSelectionCost from '../../utils/formatSelectionCost';
import formatPoints from '../../utils/formatPoints';


export default class DraftOutcome extends React.Component {
    render() {
        let { position, outcome, selection } = this.props;
        return (
            <tr className={ (position && !selection) ? 'no-selection' : '' }>
                <td className="small-width outcome-position highlight">{ position ? position.name : outcome.typeName }</td>
                { position ? (
                    <td className="roster-player-portraits">
                        { outcome ? (
                            <img src={ 'https://d161h7kqqonvwu.cloudfront.net/' + outcome.player.league + '/' + outcome.player.externalId + '.png' } />
                        ) : null }
                    </td>
                ) : null }
                <td className="text-left large-width clickable" onClick={ this.props.onOutcomeDetails.bind(this, outcome) }>
                    <a className="top-line no-break">
                        <div className="top-container">
                            <span>{ outcome ? outcome.name : '' }</span>
                        </div>
                    </a>
                </td>
                <td className="default-width">{ outcome ? outcome.player.team.name : '' }</td>
                <td className="small-width" dangerouslySetInnerHTML={ { __html: outcome ? outcome.player.team.game.label.replace(outcome.player.team.alias, '<strong>' + outcome.player.team.alias + '</strong>') : '' } }></td>
                <td className="xsmall-width">{ outcome ? formatPoints(outcome.pointsPerGame) : '-' }</td>
                <td className="smaller-width">{ outcome ? formatSelectionCost(outcome.selectionCost) : '' }</td>
                <td className="sm-btn-width unselectable clickable" onClick={ outcome ? (position ? () => { this.props.onRemoveSelection(selection) } : () => { this.props.onSelectOutcome(outcome) }) : null }>
                    { outcome ? (
                        position ? (
                            <i className="fa fa-close fa-lg"></i>
                        ) : (
                            <i className="ss-icon ss-symbolicons-block ss-right"></i>
                        )
                    ) : null }
                </td>
            </tr>
        );
    }
}
