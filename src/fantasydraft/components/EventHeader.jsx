import React from 'react';
import ContextIcon from '../components/ContextIcon';
import formatDate from '../../utils/formatDate';
import Event from '../../models/Event';
import formatCountdown from '../../utils/formatCountdown';
import formatTicketCost from '../../utils/formatTicketCost';
import formatEventPayout from '../../utils/formatEventPayout';


export default class EventHeader extends React.Component {
    componentDidMount() {
        let self = this;
        this.intervalId = setInterval(() => {
            self.forceUpdate();
        }, 1000);
    }
    componentWillUnmount() {
        clearInterval(this.intervalId);
    }
    render() {
        let event = new Event(this.props.event);
        return (
            <div className="row">
                <div className="col-md-12">
                    <div>
                        <div>
                            <div className="ig-event-header-widget styled blocked clearfix">
                                <div className="col-sm-12">
                                    <h1>
                                        <span><ContextIcon context={ event.context } /></span>
                                        { ' ' }
                                        <span>{ event.description }</span>
                                        { ' ' }
                                        <span className="event-info">
                                            <i className="fa fa-info-circle"></i>
                                        </span>
                                    </h1>
                                </div>
                                <div className="col-md-5 col-sm-5 clearfix">
                                    <div>
                                        <p>
                                            <span>BANANA</span>
                                            { ' ' }
                                            <span>BANANA</span>
                                        </p>
                                    </div>
                                    <p className="vspace-20">
                                        <a className="btn btn-positive" onClick={ () => { window.alert('CHALLENGE FRIENDS FOR EVENT ' + event.id); } }>Challenge Friends</a>
                                    </p>
                                </div>
                                <div className="col-md-4 col-sm-3">
                                    <h2 className="no-break">GAME TIME: { formatDate(event.closeMoment).toUpperCase() }</h2>
                                    { event.isUpcoming() ? (
                                        <h3>Live In: { formatCountdown(event.closeMoment) }</h3>
                                    ) : (
                                        <h3>Completed</h3>
                                    ) }
                                </div>
                                <div className="col-md-3 col-sm-4">
                                    <div className="row low-gutter">
                                        <div className="col-md-10 pool-information">
                                            <p>Entries: <var><var className="ticket-count-animated indicate-change">{ event.ticketCount }</var> / { event.ticketMax }</var></p>
                                            <p>Entry Fee: <var>{ formatTicketCost(event.ticketCost) }</var></p>
                                            <p>Prize Payout: <var>{ formatEventPayout(event.payout) }</var></p>
                                        </div>
                                    </div>
                                </div>
                                <div className="social-share hidden-xs hidden-sm">
                                    <a><i className="ss-icon ss-social-circle ss-facebook"></i></a>
                                    <a><i className="ss-icon ss-social-circle ss-googleplus"></i></a>
                                    <a><i className="ss-icon ss-social-circle ss-twitter"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}
