import React from 'react';
import formatDate from '../../utils/formatDate';


export default class DraftGamesTable extends React.Component {
    gameIsSelected(game) {
        return this.teamIsSelected(game.visitingTeam) ||
            this.teamIsSelected(game.homeTeam);
    }
    teamIsSelected(team) {
        let selectedTeamIds = this.props.selectedTeamIds;
        return _.includes(selectedTeamIds, team.id);
    }
    render() {
        let games = this.props.games || [];
        let selectedTeamIds = this.props.selectedTeamIds || [];
        return (
            <div className="game-list styled clearfix">
                <div className="col-md-12">
                    <div className="row">
                        <div className={ 'game-block all ' + (selectedTeamIds.length === 0 ? 'selected ' : '') } onClick={ this.props.onClickClear.bind(this) }>
                            <div className="game"><a className="all-games">All Games</a></div>
                        </div>
                        <div className="scrolling-box">
                            <div className="games-holder">
                                { games.map((game) => {
                                    return (
                                        <div key={ game.id } onClick={ this.props.onClickGame.bind(this, game) } className={ 'game-block ' + (this.gameIsSelected(game) ? 'selected ' : '') }>
                                            <div className="game">
                                                <div className="information">
                                                    <i className="fa fa-info-circle" onClick={ (e) => { e.stopPropagation(); this.props.onClickGameDetails(game) } }></i>
                                                    <table className="table branded headless game">
                                                        <tbody>
                                                            <tr>
                                                                <td className="team-names">
                                                                    <a className="team-name visiting" onClick={ (e) => { this.props.onClickTeam(game.visitingTeam); e.stopPropagation() } }>
                                                                        <i className={ 'fa ' + (this.teamIsSelected(game.visitingTeam) ? 'fa-check-square-o' : 'fa-square-o') }></i> <span>{ game.visitingTeam.name }</span>
                                                                    </a>
                                                                </td>
                                                                <td className="weather" rowSpan="2">
                                                                    <div className="game-box weather">
                                                                        {/* <span>
                                                                            <img src="https://d2irfdjqumt5ct.cloudfront.net/assets/img/weather/stadium_covered_22.png" className="" />
                                                                        </span> */}
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td className="team-names">
                                                                    <a className="team-name home" onClick={ (e) => { this.props.onClickTeam(game.homeTeam); e.stopPropagation() } }>
                                                                        <i className={ 'fa ' + (this.teamIsSelected(game.homeTeam) ? 'fa-check-square-o' : 'fa-square-o') }></i> <span>{ game.homeTeam.name }</span>
                                                                    </a>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td className="start-time" colSpan="2">{ formatDate(game.startMoment) }</td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                                <div className="footer" onClick={ (e) => { e.stopPropagation(); this.props.onClickGameDepthChart(game) } }>Depth Charts &nbsp;</div>
                                            </div>
                                        </div>
                                    );
                                }) }
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}
