// @flow
import React from 'react';
import { ConnectedRouter as Router } from 'react-router-redux';
import { Route, Switch, Redirect } from 'react-router-dom';
import { connect, history } from '../../redux';

import RouterScrollToTop from '../../components/RouterScrollToTop';
import SiteDesign from './SiteDesign';

import AboutPage from '../pages/AboutPage';
import TermsOfUsePage from '../pages/TermsOfUsePage';
import PrivacyPolicyPage from '../pages/PrivacyPolicyPage';
import EventsPage from '../pages/EventsPage';
import LandingPage from '../pages/LandingPage';
import EventPage from '../pages/EventPage';
import TicketPage from '../pages/TicketPage';
import NotFoundPage from '../pages/NotFoundPage';
import PressPage from '../pages/PressPage';
import SupportPage from '../pages/SupportPage';
import FrequentQuestionsPage from '../pages/FrequentQuestionsPage';
import ScoringAndRulesPage from '../pages/ScoringAndRulesPage';
import ContestTypesPage from '../pages/ContestTypesPage';
import CreateContestPage from '../pages/CreateContestPage';
import FantasyDraftBasicsPage from '../pages/FantasyDraftBasicsPage';
import MyReferralsPage from '../pages/MyReferralsPage';
import ReferralsPage from '../pages/ReferralsPage';
import DepositPage from '../pages/DepositPage';
import WithdrawPage from '../pages/WithdrawPage';
import MyLineupsPage from '../pages/MyLineupsPage';
import MyContestsPage from '../pages/MyContestsPage';
import LoginPage from '../pages/LoginPage';
import RegisterPage from '../pages/RegisterPage';
import ForgotPasswordPage from '../pages/ForgotPasswordPage';
import ResetPasswordPage from '../pages/ResetPasswordPage';
import CreateEventPage from '../pages/CreateEventPage';
import AccountSummaryPage from '../pages/AccountSummaryPage';
import AccountSettingsPage from '../pages/AccountSettingsPage';
import MyReferralsReportPage from '../pages/MyReferralsReportPage';
import DepositLimitsPage from '../pages/DepositLimitsPage';
import TemplatePage from '../pages/TemplatePage';


const ConnectedSiteDesign = connect(SiteDesign);


export default class Application extends React.Component {
    state: Object;

    constructor() {
        super();
        this.state = {
            activeCustomer: null
        };
    }
    componentWillReceiveProps(nextProps: Object) {
        // If the Customer logged out, go back to the splash page
        let nextCustomer = nextProps.store.customer.activeCustomer;

        if (this.state.activeCustomer && !nextCustomer) {
            this.props.actions.router.push('/');
        }

        this.setState({
            activeCustomer: nextCustomer
        });
    }
    componentDidMount() {
        let self = this;
        setInterval(() => {
            self.props.actions.events.fetchEvents();
            if (this.props.store.customer.activeCustomer) {
                self.props.actions.tickets.fetchTickets();
                self.props.actions.templates.fetchTemplates();
            }
        }, 30000);
        self.props.actions.events.fetchEvents();
        self.props.actions.customer.fetchActiveCustomer().then(() => {
            self.props.actions.tickets.fetchTickets();
            self.props.actions.templates.fetchTemplates();
        });
    }
    render() {
        return (
            <Router history={ history }>
                <ConnectedSiteDesign>
                    <Switch>
                        <Route exact path="/" component={ connect(LandingPage) } />

                        <Route exact path="/login/" component={ connect(LoginPage) } />
                        <Route exact path="/register/" component={ connect(RegisterPage) } />
                        <Route exact path="/forgot-password/" component={ connect(ForgotPasswordPage) } />
                        <Route exact path="/reset-password/" component={ connect(ResetPasswordPage) } />

                        <Route exact path="/deposit/" component={ connect(DepositPage) } />
                        <Route exact path="/withdraw/" component={ connect(WithdrawPage) } />

                        <Route exact path="/create-a-contest/" component={ connect(CreateContestPage) } />
                        <Route exact path="/contests/" component={ connect(EventsPage) } />
                        <Route exact path="/contest/:id/" render={ ({ match }) => {
                            return (
                                <EventPage { ...this.props } eventId={ match.params.id } />
                            );
                        } } />
                        <Route exact path="/template/:id/" render={ ({ match }) => {
                            return (
                                <TemplatePage { ...this.props } templateId={ match.params.id } />
                            );
                        } } />
                        <Route exact path="/ticket/:id/" render={ ({ match }) => {
                            return (
                                <TicketPage { ...this.props } ticketId={ match.params.id } />
                            );
                        } } />
                        <Route exact path="/my-referrals/" component={ connect(MyReferralsPage) } />
                        <Route exact path="/my-referrals/group-report/" component={ connect(MyReferralsReportPage) } />
                        <Route exact path="/my-contests/" component={ connect(MyContestsPage) } />
                        <Route exact path="/my-lineups/" render={ () => {
                            return (
                                <MyLineupsPage { ...this.props } />
                            );
                        } } />

                        <Route exact path="/create-a-contest/" component={ connect(CreateEventPage) } />

                        <Route exact path="/profile/" render={ () => {
                            return (
                                <Redirect to="/profile/account/" />
                            );
                        } } />
                        <Route exact path="/profile/account/" component={ connect(AccountSummaryPage) } />
                        <Route exact path="/profile/settings/" component={ connect(AccountSettingsPage) } />
                        <Route exact path="/profile/limits/" component={ connect(DepositLimitsPage) } />

                        <Route exact path="/referrals/" component={ ReferralsPage } />
                        <Route exact path="/about/" component={ AboutPage } />
                        <Route exact path="/about/terms-of-use/" component={ TermsOfUsePage } />
                        <Route exact path="/about/privacy-policy/" component={ PrivacyPolicyPage } />
                        <Route exact path="/about/press/" component={ PressPage } />

                        <Route exact path="/help-center/" render={ () => {
                            return (
                                <Redirect to="/help-center/fantasydraft-basics/" />
                            );
                        } } />
                        <Route exact path="/help-center/fantasydraft-basics/" component={ FantasyDraftBasicsPage } />
                        <Route exact path="/help-center/faqs/" component={ FrequentQuestionsPage } />
                        <Route exact path="/help-center/support/" component={ SupportPage } />
                        <Route exact path="/help-center/contest-types/" component={ ContestTypesPage } />
                        <Route exact path="/help-center/scoring-and-rules/" component={ ScoringAndRulesPage } />

                        <Route component={ NotFoundPage } />
                    </Switch>
                    <RouterScrollToTop />
                </ConnectedSiteDesign>
            </Router>
        );
    }
}

Application.propTypes = {
    actions: React.PropTypes.shape({
        customer: React.PropTypes.shape({
            loginCustomer: React.PropTypes.func,
            logoutCustomer: React.PropTypes.func,
        }),
        router: React.PropTypes.shape({
            push: React.PropTypes.func
        })
    }),
    store: React.PropTypes.shape({
        customer: React.PropTypes.shape({
            activeCustomer: React.PropTypes.object,
            fetchingActiveCustomer: React.PropTypes.bool
        }),
        events: React.PropTypes.shape({
            byId: React.PropTypes.object.isRequired
        })
    })
};
