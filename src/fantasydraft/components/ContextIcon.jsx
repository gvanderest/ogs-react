import React from 'react';


export default class ContextIcon extends React.Component {
    getClasses() {
        let context = this.props.context;
        let classes = ['sport-icon'];
        if (this.props.invert) {
            classes.push('sport-icon-alt');
        }
        classes.push({
            'NBA': 'basketball',
            'MLB': 'baseball',
            'PGA': 'golf',
            'NFL': 'football',
            'NHL': 'hockey'
        }[context]);
        return classes.join(' ');
    }
    render() {
        return (
            <i className={ this.getClasses() }></i>
        );
    }
}


ContextIcon.propTypes = {
    context: React.PropTypes.string.isRequired
}
