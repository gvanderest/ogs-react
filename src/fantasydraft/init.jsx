import React from 'react';
import ReactDOM from 'react-dom';
import Application from './components/Application';
import { Provider } from 'react-redux';
import { store, connect } from '../redux';


let ConnectedApplication = connect(Application);

ReactDOM.render(
    <Provider store={ store }>
        <ConnectedApplication />
    </Provider>,
    document.getElementById('application')
);
