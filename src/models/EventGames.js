import { store } from '../redux';
import moment from 'moment';


export default class EventGames {
    constructor(data) {
        Object.assign(this, data);
    }
    isOpen() {
        let now = moment();
        return now.isBefore(this.closeMoment);
    }
    isFinalized() {
        let now = moment();
        return !now.isBefore(this.finalizeMoment);
    }
    isClosed() {
        return !this.isOpen() && !this.isFinalized();
    }
}
