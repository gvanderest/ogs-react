import { store } from '../redux';
import EventGames from './EventGames';


export default class Template {
    constructor(data) {
        Object.assign(this, data);
    }
    getEventGames() {
        let state = store.getState();
        let eventGames = state.eventGames.byId[this.externalId];
        if (!eventGames) {
            return null;
        }
        return new EventGames(eventGames);
    }
}
