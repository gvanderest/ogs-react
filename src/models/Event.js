import moment from 'moment';


export default class Event {
    constructor(data) {
        Object.assign(this, data);
    }
    isGuaranteed() {
        return this.ticketMin === 1;
    }
    isMultiEntry() {
        return this.ticketMaxPerUser > 1;
    }
    isUpcoming() {
        let now = moment();
        return now.isBefore(this.closeMoment);
    }
}
