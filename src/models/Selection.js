export default class Selection {
    constructor(data) {
        Object.assign(this, data);
    }
    scoreIsDropped() {
        return !!this.scoreDropped;
    }
}
