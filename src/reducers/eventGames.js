import {
    FETCHED_EVENTGAMES_DETAILS
} from '../actions/eventGames';

const initialState = {
    byId: {}
};


function handleFetchedEventGamesDetails(store: Object, eventGames) {
    return {
        ...store,
        byId: {
            ...store.byId,
            [eventGames.id]: {
                ...store.byId[eventGames.id],
                ...eventGames
            }
        }
    };
}


export default function(store: Object = initialState, action: Object = {}) {
    let type: string = action.type;
    switch (type) {
        case FETCHED_EVENTGAMES_DETAILS: {
            return handleFetchedEventGamesDetails(store, action.eventGames);
        }
        default: {
            return store;
        }
    }
}
