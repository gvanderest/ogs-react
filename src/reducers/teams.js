import {
    FETCHED_TEAMS
} from '../actions/teams';

const initialState = {
    byId: {}
};


function handleFetchedTeams(store: Object, action: Object) {
    let teams = action.teams;
    let newStore = { ...store };
    teams.forEach((team) => {
        newStore.byId[team.id] = {
            ...store.byId[team.id],
            ...team
        }
    });
    return newStore;
}


export default function(store: Object = initialState, action: Object = {}) {
    let type: string = action.type;
    switch (type) {
        case FETCHED_TEAMS: {
            return handleFetchedTeams(store, action);
        }
        default: {
            return store;
        }
    }
}
