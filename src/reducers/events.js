import {
    FETCHED_BASIC_EVENTS,
    FETCHED_EVENT_DETAILS
} from '../actions/events';

const initialState = {
    byId: {}
};


function handleFetchedBasicEvents(store: Object, action: Object) {
    let newStore = { ...store };

    let events: Array<Object> = action.events;
    events.forEach((event: Object) => {
        let existingEvent: Object = newStore.byId[event.id] || {};
        let newEvent = {
            ...existingEvent,
            ...event
        };
        newStore.byId[event.id] = newEvent;
    });

    return newStore;
}


function handleFetchedEventDetails(store: Object, action: Object) {
    let event = action.event;
    return {
        ...store,
        byId: {
            ...store.byId,
            [event.id]: {
                ...store.byId[event.id],
                ...event
            }
        }
    };
}


export default function(store: Object = initialState, action: Object = {}) {
    let type: string = action.type;
    switch (type) {
        case FETCHED_BASIC_EVENTS: {
            return handleFetchedBasicEvents(store, action);
        }
        case FETCHED_EVENT_DETAILS: {
            return handleFetchedEventDetails(store, action);
        }
        default: {
            return store;
        }
    }
}
