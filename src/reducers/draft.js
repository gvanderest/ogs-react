import {
    START_DRAFTING_EVENT,
    STOP_DRAFTING_EVENT
} from '../actions/draft';

const initialState = {
    byId: {}
};


export default function(store: Object = initialState, action: Object = {}) {
    switch (action.type) {
        default: {
            return store;
        }
    }
}
