import {
    FETCHED_BASIC_TICKETS
} from '../actions/tickets';

const initialState = {
    byId: {}
};


function handleFetchedTicketDetails(store, action) {
    let { ticket } = action;
    let newStore = { ...store };

    let existingTicket: Object = newStore.byId[ticket.id] || {};
    let newTicket = {
        ...existingTicket,
        ...ticket
    };
    newStore.byId[ticket.id] = newTicket;
    return newStore;
}


function handleFetchedBasicTickets(store: Object, action: Object) {
    let newStore = store;

    let tickets: Array<Object> = action.tickets;
    tickets.forEach((ticket: Object) => {
        newStore = handleFetchedTicketDetails(newStore, { ticket });
    });

    return newStore;
}




export default function(store: Object = initialState, action: Object = {}) {
    let type: string = action.type;
    switch (type) {
        case FETCHED_BASIC_TICKETS: {
            return handleFetchedBasicTickets(store, action);
        }
        default: {
            return store;
        }
    }
}
