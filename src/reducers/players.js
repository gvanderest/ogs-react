import {
    FETCHED_PLAYERS
} from '../actions/players';

const initialState = {
    byId: {}
};


function handleFetchedPlayers(store: Object, action: Object) {
    let players = action.players;
    let newStore = { ...store };
    players.forEach((player) => {
        newStore.byId[player.id] = {
            ...store.byId[player.id],
            ...player
        }
    });
    return newStore;
}


export default function(store: Object = initialState, action: Object = {}) {
    let type: string = action.type;
    switch (type) {
        case FETCHED_PLAYERS: {
            return handleFetchedPlayers(store, action);
        }
        default: {
            return store;
        }
    }
}
