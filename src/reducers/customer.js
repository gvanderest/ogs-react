import {
    FETCHING_ACTIVE_CUSTOMER,
    FETCHED_ACTIVE_CUSTOMER
} from '../actions/customer';

const initialState = {
    fetchingActiveCustomer: false,
    activeCustomer: null,
    csrfToken: null
};


function handleFetchingCustomer(state, action) {
    return {
        ...state,
        fetchingActiveCustomer: true
    };
}


function handleFetchedCustomer(state, action) {
    return {
        ...state,
        activeCustomer: action.customer,
        fetchingActiveCustomer: false
    };
}


export default function(state: Object = initialState, action: Object = {}) {
    switch (action.type) {
        case FETCHING_ACTIVE_CUSTOMER: {
            return handleFetchingCustomer();
        }
        case FETCHED_ACTIVE_CUSTOMER: {
            return handleFetchedCustomer(state, action);
        }
        default: {
            return state;
        }
    }
}
