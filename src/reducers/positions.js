import {
    FETCHED_POSITIONS
} from '../actions/positions';

const initialState = {
    byId: {}
};


function handleFetchedPositions(store: Object, action: Object) {
    let positions = action.positions;
    let newStore = { ...store };
    positions.forEach((position) => {
        newStore.byId[position.id] = {
            ...store.byId[position.id],
            ...position
        }
    });
    return newStore;
}


export default function(store: Object = initialState, action: Object = {}) {
    let type: string = action.type;
    switch (type) {
        case FETCHED_POSITIONS: {
            return handleFetchedPositions(store, action);
        }
        default: {
            return store;
        }
    }
}
