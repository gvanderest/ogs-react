import {
    FETCHED_GAMES
} from '../actions/games';

const initialState = {
    byId: {}
};


function handleFetchedGames(store: Object, action: Object) {
    let games = action.games;
    let newStore = { ...store };
    games.forEach((game) => {
        newStore.byId[game.id] = {
            ...store.byId[game.id],
            ...game
        }
    });
    return newStore;
}


export default function(store: Object = initialState, action: Object = {}) {
    let type: string = action.type;
    switch (type) {
        case FETCHED_GAMES: {
            return handleFetchedGames(store, action);
        }
        default: {
            return store;
        }
    }
}
