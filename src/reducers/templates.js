import {
    FETCHING_TEMPLATES,
    FETCHED_TEMPLATES,
    DELETED_TEMPLATE
} from '../actions/templates';

const initialState = {
    byId: {},
    fetchingTemplates: false
};


function handleFetchedTemplate(state, template) {
    return {
        ...state,
        byId: {
            ...state.byId,
            [template.id]: {
                ...state.byId[template.id],
                ...template
            }
        }
    };
}


function handleFetchedTemplates(state, templates) {
    state = {
        ...state,
        fetchingTemplates: false,
        byId: {}
    };
    templates.forEach((template) => {
        state = handleFetchedTemplate(state, template);
    });
    return state;
}


function handleDeletedTemplate(state, templateId) {
    state = {
        ...state,
        byId: {
            ...state.byId
        }
    };

    delete state.byId[templateId];

    return state;
}


function handleFetchingTemplates(state) {
    return {
        ...state,
        fetchingTemplates: true
    };
}




export default function(state: Object = initialState, action: Object = {}) {
    switch (action.type) {
        case FETCHING_TEMPLATES: {
            return handleFetchingTemplates(state);
        }
        case FETCHED_TEMPLATES: {
            let { templates } = action;
            return handleFetchedTemplates(state, templates);
        }
        case DELETED_TEMPLATE: {
            let { id } = action;
            return handleDeletedTemplate(state, id);
        }
        default: {
            return state;
        }
    }
}
