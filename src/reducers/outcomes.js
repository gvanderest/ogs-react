import {
    FETCHED_OUTCOMES
} from '../actions/outcomes';

const initialState = {
    byId: {}
};


function handleFetchedOutcomes(store: Object, action: Object) {
    let outcomes = action.outcomes;
    let newStore = { ...store };
    outcomes.forEach((outcome) => {
        newStore.byId[outcome.id] = {
            ...store.byId[outcome.id],
            ...outcome
        }
    });
    return newStore;
}


export default function(store: Object = initialState, action: Object = {}) {
    let type: string = action.type;
    switch (type) {
        case FETCHED_OUTCOMES: {
            return handleFetchedOutcomes(store, action);
        }
        default: {
            return store;
        }
    }
}
