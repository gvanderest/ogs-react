export const API_URL = 'https://qa7.fantasydraft.com/api/';
export const MOMENT_TIMEZONE = 'America/New_York';
export const MOMENT_TIMEZONE_LABEL = 'ET';
export const MOMENT_DATE_FORMAT = `MMM D @ h:mm A [${ MOMENT_TIMEZONE_LABEL }]`

export const CONTEXTS = [
    { id: 'MLB', context: 'MLB', label: 'MLB', iconClasses: 'sport-icon mlb', sortOrder: 0 },
    { id: 'PGA', context: 'PGA', label: 'PGA', iconClasses: 'sport-icon pga', sortOrder: 1 },
    { id: 'NBA', context: 'NBA', label: 'NBA', iconClasses: 'sport-icon nba', sortOrder: 2 },
    { id: 'NHL', context: 'NHL', label: 'NHL', iconClasses: 'sport-icon nhl', sortOrder: 3 },
    { id: 'NFL', context: 'NFL', label: 'NFL', iconClasses: 'sport-icon mlb', sortOrder: 4 },
];
