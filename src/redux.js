import { connect as reduxConnect } from 'react-redux';
import { bindActionCreators } from 'redux'
import { push } from 'react-router-redux';
import { createStore, combineReducers, applyMiddleware } from 'redux'
import createHistory from 'history/createBrowserHistory'
import { routerReducer, routerMiddleware } from 'react-router-redux'
import { composeWithDevTools } from 'redux-devtools-extension';
import thunkMiddleware from 'redux-thunk';


export const history = createHistory();


import customerReducer from './reducers/customer';
import eventsReducer from './reducers/events';
import eventGamesReducer from './reducers/eventGames';
import gamesReducer from './reducers/games';
import outcomesReducer from './reducers/outcomes';
import playersReducer from './reducers/players';
import positionsReducer from './reducers/positions';
import teamsReducer from './reducers/teams';
import ticketsReducer from './reducers/tickets';
import templatesReducer from './reducers/templates';


export const store = createStore(
    combineReducers({
        customer: customerReducer,
        events: eventsReducer,
        eventGames: eventGamesReducer,
        outcomes: outcomesReducer,
        players: playersReducer,
        teams: teamsReducer,
        games: gamesReducer,
        positions: positionsReducer,
        router: routerReducer,
        tickets: ticketsReducer,
        templates: templatesReducer,
    }),
    composeWithDevTools(
        applyMiddleware(
            routerMiddleware(history),
            thunkMiddleware
        )
    )
)


import * as customerActions from './actions/customer';
import * as eventsActions from './actions/events';
import * as draftActions from './actions/draft';
import * as ticketsActions from './actions/tickets';
import * as templatesActions from './actions/templates';


// Map all actions below an "actions" node
function mapDispatchToProps(dispatch: Function) {
    return {
        actions: {
            customer: bindActionCreators({ ...customerActions }, dispatch),
            events: bindActionCreators({ ...eventsActions }, dispatch),
            draft: bindActionCreators({ ...draftActions }, dispatch),
            router: bindActionCreators({ push: push }, dispatch),
            tickets: bindActionCreators({ ...ticketsActions }, dispatch),
            templates: bindActionCreators({ ...templatesActions }, dispatch),
        }
    };
}

// Map to be below a "store" node
export const connect = reduxConnect(function(store: Object) {
    return {
        store
    };
}, mapDispatchToProps);
