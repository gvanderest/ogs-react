import Promise from 'promise';
import { API_URL } from '../settings';
import { fetchedBasicEvents, fetchedEventDetails } from './events';
import moment from 'moment';


export const FETCHING_BASIC_TICKETS: string = 'FETCHING_BASIC_TICKETS';
export const FETCHED_BASIC_TICKETS: string = 'FETCHED_BASIC_TICKETS';
export const ERROR_FETCHING_BASIC_TICKETS: string = 'ERROR_FETCHING_BASIC_TICKETS';
export const FETCHING_TICKET_DETAILS: string = 'FETCHING_TICKET_DETAILS';
export const FETCHED_TICKET_DETAILS: string = 'FETCHED_TICKET_DETAILS';
export const ERROR_FETCHING_TICKET_DETAILS: string = 'ERROR_FETCHING_TICKET_DETAILS';


export function fetchingBasicTickets() {
    return {
        type: FETCHING_BASIC_TICKETS
    };
}

export function fetchedBasicTickets(tickets: Array<Object>) {
    return {
        type: FETCHED_BASIC_TICKETS,
        tickets
    };
}

export function errorFetchingBasicTickets() {
    return {
        type: ERROR_FETCHING_BASIC_TICKETS
    };
}


export function fetchTickets() {
    return (dispatch: Function) => {
        dispatch(fetchingBasicTickets());

        let promise = new Promise((yes, no) => {
            return fetch(`${ API_URL }v1/tickets/?event__finalize_ts__gte=2017-01-19`, {
                credentials: 'include'
            }).then((response) => {
                response.json().then((rawTickets: Array<Object>) => {
                    let events = [];
                    let tickets: Array<Object> = rawTickets.objects.map((rawTicket: Object) => {
                        let rawEvent = rawTicket.event;
                        events.push({
                            id: String(rawEvent.id),
                            context: rawEvent.context,
                            description: rawEvent.description,
                            externalId: String(rawEvent.external_id),
                            closeTimestamp: moment.utc(rawEvent.close_ts).unix(),
                            closeMoment: moment.utc(rawEvent.close_ts),
                            allowedGroups: rawEvent.entry_groups,
                            allowedProvinces: rawEvent.entry_provs,
                            eventGamesConfigName: rawEvent.eventgamesconfig,
                            finalizeTimestamp: moment.utc(rawEvent.finalize_ts).unix(),
                            finalizeMoment: moment.utc(rawEvent.finalize_ts),
                            restrictedGroups: rawEvent.restrict_groups,
                            restrictedProvinces: rawEvent.restrict_provs,
                            status: rawEvent.status,
                            payout: rawEvent.payout,
                            payourCurrency: rawEvent.payout_currency,
                            ticketCost: rawEvent.ticket_cost,
                            ticketCostCurrency: rawEvent.ticket_currency,
                            ticketCount: rawEvent.ticket_count,
                            ticketMax: rawEvent.ticket_max,
                            ticketMaxPerUser: rawEvent.ticket_max_per_user,
                            ticketMin: rawEvent.ticket_min
                        });

                        return {
                            id: String(rawTicket.id),
                            eventId: String(rawTicket.event_id),
                            templateId: String(rawTicket.template_id),
                            positionTied: rawTicket.position_tied,
                            status: rawTicket.status,
                            pointsEarned: rawTicket.points_earned,
                            settings: rawTicket.settings_json ? JSON.parse(rawTicket.settings_json) : {},
                            profit: rawTicket.profit,
                            cost: rawTicket.cost,
                            costCurrency: rawTicket.cost_currency,
                            selectionCurrencyRemaining: rawTicket.selection_currency_remaining,
                            eventScoringTimeIndicator: rawTicket.event_scoring_time_indicator,
                            position: rawTicket.position,
                            amountWon: rawTicket.amount_won
                        };
                    });
                    dispatch(fetchedBasicEvents(events))
                    yes(tickets);
                }, no);
            }, no);
        })

        promise.then((tickets) => {
            dispatch(fetchedBasicTickets(tickets));
        }, () => {
            dispatch(errorFetchingBasicTickets());
        });

        return promise;
    };
}

export function fetchingTicketDetails(id) {
    return {
        type: FETCHING_TICKET_DETAILS,
        id
    };
}

export function fetchedTicketDetails(ticket) {
    return {
        type: FETCHED_TICKET_DETAILS,
        ticket
    };
}

export function errorFetchingTicketDetails(id) {
    return {
        type: ERROR_FETCHING_TICKET_DETAILS,
        id
    };
}

export function fetchTicketDetails(id) {
    return (dispatch) => {
        dispatch(fetchingTicketDetails(id));

        let promise = new Promise((yes, no) => {
            fetch(`${ API_URL }v1/tickets/${ id }/`, {
                credentials: 'include'
            }).then((rawTicketResponse) => {
                rawTicketResponse.json().then((rawTicket) => {
                    let ticket = {
                        id: String(rawTicket.id),
                        cost: rawTicket.cost,
                        costCurrency: rawTicket.cost_currency,
                        createdTimestamp: moment.utc(rawTicket.created_ts).unix(),
                        createdMoment: moment.utc(rawTicket.created_ts),
                        pointsEarned: rawTicket.points_earned,
                        position: rawTicket.position,
                        positionTied: rawTicket.position_tied,
                        profit: rawTicket.profit,
                        selectionCurrencyRemaining: rawTicket.selection_currency_remaining,
                        settings: rawTicket.settings_json ? JSON.parse(rawTicket.settings_json) : {},
                        status: rawTicket.status,
                        templateId: String(rawTicket.template_id),
                        userId: String(rawTicket.user_id),
                        username: rawTicket.username,
                    };
                    // FIXME convert event
                    let event = rawTicket.event;
                    event.id = String(event.id);
                    // FIXME convert event
                    dispatch(fetchedEventDetails(event));
                    yes(ticket);
                }, no);
            }, no);
        });

        promise.then((ticket) => {
            dispatch(fetchedTicketDetails(ticket));
        }, () => {
            dispatch(errorFetchingTicketDetails());
        });

        return promise;
    };
}
