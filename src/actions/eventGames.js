import { fetchedOutcomes } from './outcomes';
import { fetchedPositions } from './positions';
import { fetchedGames } from './games';
import { fetchedPlayers } from './players';
import { fetchedTeams } from './teams';
import Promise from 'promise';
import { API_URL } from '../settings';
import moment from 'moment';


export const FETCHING_EVENTGAMES_DETAILS = 'FETCHING_EVENTGAMES_DETAILS';
export const FETCHED_EVENTGAMES_DETAILS = 'FETCHED_EVENTGAMES_DETAILS';
export const ERROR_FETCHING_EVENTGAMES_DETAILS = 'ERROR_FETCHING_EVENTGAMES_DETAILS';


export function fetchingEventGamesDetails(id) {
    return {
        type: FETCHING_EVENTGAMES_DETAILS,
        id
    };
}


export function fetchedEventGamesDetails(eventGames) {
    return {
        type: FETCHED_EVENTGAMES_DETAILS,
        eventGames
    };
}


export function errorFetchingEventGamesDetails(id) {
    return {
        type: ERROR_FETCHING_EVENTGAMES_DETAILS,
        id
    };
}


export function fetchEventGamesDetails(id) {
    return (dispatch) => {
        dispatch(fetchingEventGamesDetails(id));

        let promise = new Promise((yes, no) => {
            fetch(`${ API_URL }v1/fantasy/eventgames/${ id }/`).then((response) => {
                response.json().then((rawEventGames) => {
                    let outcomes = Object.values(rawEventGames.o).map((rawOutcome) => {
                        return {
                            id: String(rawOutcome.i),
                            name: rawOutcome.n,
                            externalId: rawOutcome.ei,
                            typeName: rawOutcome.t,
                            selectionCost: rawOutcome.sc,
                            pointsAvailable: rawOutcome.pa,
                            pointsPerGame: rawOutcome.pp,
                            closeTimestamp: moment.utc(rawOutcome.ct).unix(),
                            closeMoment: moment.utc(rawOutcome.ct),
                        };
                    });
                    dispatch(fetchedOutcomes(outcomes));

                    let positions = Object.values(rawEventGames.evp).map((rawPosition) => {
                        return {
                            id: String(rawPosition.i),
                            status: rawPosition.s,
                            sortOrder: rawPosition.so,
                            outcomeTypeNames: rawPosition.o,
                            name: rawPosition.n
                        };
                    });
                    dispatch(fetchedPositions(positions));

                    let players = [];
                    let teams = [];
                    let games = []

                    Object.values(rawEventGames.g).forEach((rawGame) => {
                        Object.keys(rawGame.p).forEach((rawPlayerId) => {
                            let rawPlayer = rawGame.p[rawPlayerId];
                            players.push({
                                id: String(rawPlayerId),
                                handedness: rawPlayer.h,
                                battingHandedness: rawPlayer.bh,
                                teamId: String(rawPlayer.t),
                                starter: rawPlayer.is,
                                league: rawGame.l,
                                externalId: String(rawPlayer.ei)
                            });
                        });

                        teams.push({
                            id: String(rawGame.hti),
                            alias: rawGame.hta,
                            name: rawGame.htn,
                            score: rawGame.hts, // FIXME What is this?
                            gameId: String(rawGame.i)
                        });

                        teams.push({
                            id: String(rawGame.vti),
                            alias: rawGame.vta,
                            name: rawGame.vtn,
                            score: rawGame.vts, // FIXME What is this?
                            gameId: String(rawGame.i)
                        });

                        games.push({
                            id: String(rawGame.i),
                            league: rawGame.l,
                            label: (rawGame.vta + ' @ ' + rawGame.hta),
                            status: rawGame.s,
                            startTimestamp: moment.utc(rawGame.gt).unix(),
                            startMoment: moment.utc(rawGame.gt),
                            dateString: rawGame.d,
                            homeTeamId: rawGame.hti,
                            visitingTeamId: rawGame.vti,
                            eventGamesId: rawEventGames.i
                        });
                    });

                    dispatch(fetchedGames(games));
                    dispatch(fetchedPlayers(players));
                    dispatch(fetchedTeams(teams));

                    yes({
                        id: String(id),
                        gameIds: Object.keys(rawEventGames.g),
                        context: rawEventGames.cxt,
                        configurationName: rawEventGames.cfg,
                        selectionCurrency: rawEventGames.sc,
                        closeTimestamp: rawEventGames.c,
                        outcomeIds: Object.keys(rawEventGames.o),
                        positionIds: Object.keys(rawEventGames.evp)
                    });
                }, no);
            }, no);
        });

        promise.then((eventGames) => {
            dispatch(fetchedEventGamesDetails(eventGames));
        }, () => {
            dispatch(errorFetchingEventGamesDetails(id));
        });

        return promise;
    };
}
