import _ from 'lodash';
import moment from 'moment';
import Promise from 'promise';
import { API_URL } from '../settings';
import { fetchedEventGamesDetails } from './eventGames';
import hydrateFetchCSRFToken from '../utils/hydrateFetchCSRFToken';


export const FETCHING_TEMPLATES = 'FETCHING_TEMPLATES';
export const FETCHED_TEMPLATES = 'FETCHED_TEMPLATES';
export const ERROR_FETCHING_TEMPLATES = 'ERROR_FETCHING_TEMPLATES';
export const DELETING_TEMPLATE = 'DELETING_TEMPLATE';
export const ERROR_DELETING_TEMPLATE = 'ERROR_DELETING_TEMPLATE';
export const DELETED_TEMPLATE = 'DELETED_TEMPLATE';


export function fetchingTemplates() {
    return {
        type: FETCHING_TEMPLATES
    };
}


export function fetchedTemplates(templates) {
    return {
        type: FETCHED_TEMPLATES,
        templates
    };
}


export function errorFetchingTemplates() {
    return {
        type: ERROR_FETCHING_TEMPLATES
    };
}


export function fetchTemplates() {
    return (dispatch) => {
        let promise = new Promise((yes, no) => {
            dispatch(fetchingTemplates());

            fetch(`${ API_URL }v1/tickets/sportstemplates/`, {
                credentials: 'include'
            }).then((response) => {
                response.json().then((templatesData) => {
                    let templates = [];
                    templatesData.objects.forEach((rawEventGames) => {
                        let eventGames = rawEventGames.eventGamesCollection;
                        eventGames.closeMoment = moment.utc(eventGames.closeEvent);
                        eventGames.closeTimestamp = eventGames.closeMoment.unix();
                        eventGames.finalizeMoment = moment.utc(eventGames.finalizeEvent);
                        eventGames.finalizeTimestamp = eventGames.finalizeMoment.unix();
                        dispatch(fetchedEventGamesDetails(eventGames));

                        rawEventGames.templates.forEach((rawTemplate) => {
                            // FIXME clean up some fields
                            let minimumSelectionModifiedTs = _.min(_.map(rawTemplate.selections, 'modifiedTs'));
                            rawTemplate.ticketIds = rawTemplate.tickets.map((ticketUrl) => {
                                return ticketUrl.replace(/\/api\/v1\/tickets\/(\d+)\//, '$1');
                            });
                            rawTemplate.id = String(rawTemplate.id);
                            rawTemplate.externalId = String(rawTemplate.externalId);
                            rawTemplate.userId = String(rawTemplate.userId);
                            rawTemplate.modifiedMoment = moment.utc(minimumSelectionModifiedTs);
                            rawTemplate.context = eventGames.context;
                            templates.push(rawTemplate);
                        });
                    });
                    yes(templates);
                }, no);
            }, no);
        });

        promise.then((templates) => {
            dispatch(fetchedTemplates(templates));
        }, () => {
            dispatch(errorFetchingTemplates());
        });

        return promise;
    };
}


export function deletingTemplate(id) {
    return {
        type: DELETING_TEMPLATE,
        id
    };
}


export function errorDeletingTemplate(id) {
    return {
        type: ERROR_DELETING_TEMPLATE,
        id
    };
}


export function deletedTemplate(id) {
    return {
        type: DELETED_TEMPLATE,
        id
    };
}


export function deleteTemplate(id) {
    return (dispatch) => {
        let promise = new Promise((yes, no) => {
            dispatch(deletingTemplate(id));

            let options = hydrateFetchCSRFToken({
                method: 'DELETE',
                credentials: 'include',
            });

            fetch(`${ API_URL }v1/tickets/templates/${ id }/`, options).then((response) => {
                if (response.ok) {
                    yes();
                } else {
                    no();
                }
            }, no);
        });

        promise.then(() => {
            dispatch(deletedTemplate(id));
        }, () => {
            dispatch(errorDeletingTemplate(id));
        });

        return promise;
    };
}
