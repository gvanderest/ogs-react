export const START_DRAFTING_EVENT: string = 'START_DRAFTING_EVENT';
export const STOP_DRAFTING_EVENT: string = 'STOP_DRAFTING_EVENT';


import { fetchEventDetails } from './events';
import { fetchEventGamesDetails } from './eventGames';
import Promise from 'promise';


export function startDraftingEvent(id) {
    return (dispatch) => {
        var promise = new Promise((yes, no) => {
            dispatch(fetchEventDetails(id)).then((event) => {
                dispatch(fetchEventGamesDetails(event.externalId)).then(() => {
                    yes(event);
                }, no);
            }, no);
        });

        return promise;
    };
}

export function stopDraftingEvent(id) {
    return {
        type: STOP_DRAFTING_EVENT,
        id
    }
}
