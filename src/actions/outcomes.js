export const FETCHED_OUTCOMES = 'FETCHED_OUTCOMES';


export function fetchedOutcomes(outcomes) {
    return {
        type: FETCHED_OUTCOMES,
        outcomes
    };
}
