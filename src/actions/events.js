import moment from 'moment';
import Promise from 'promise';
import { API_URL } from '../settings';


export const FETCH_EVENTS: string = 'FETCH_EVENTS';
export const FETCHING_BASIC_EVENTS: string = 'FETCHING_BASIC_EVENTS';
export const FETCHED_BASIC_EVENTS: string = 'FETCHED_BASIC_EVENTS';
export const ERROR_FETCHING_BASIC_EVENTS: string = 'ERROR_FETCHING_BASIC_EVENTS';
export const FETCHING_EVENT_DETAILS: string = 'FETCHING_EVENT_DETAILS';
export const FETCHED_EVENT_DETAILS: string = 'FETCHED_EVENT_DETAILS';
export const ERROR_FETCHING_EVENT_DETAILS: string = 'ERROR_FETCHING_EVENT_DETAILS';


export function fetchingBasicEvents() {
    return {
        type: FETCHING_BASIC_EVENTS
    };
}

export function fetchedBasicEvents(events: Array<Object>) {
    return {
        type: FETCHED_BASIC_EVENTS,
        events
    };
}

export function errorFetchingBasicEvents() {
    return {
        type: ERROR_FETCHING_BASIC_EVENTS
    };
}


export function fetchingEventDetails(id) {
    return {
        type: FETCHING_EVENT_DETAILS,
        id: id
    }
}

export function errorFetchingEventDetails(id) {
    return {
        type: ERROR_FETCHING_EVENT_DETAILS,
        id: id
    }
}

export function fetchedEventDetails(event) {
    return {
        type: FETCHED_EVENT_DETAILS,
        event
    }
}

export function fetchEventDetails(id) {
    return (dispatch: Function, getStore) => {

        // Already fetched in the past
        let existingEvent = getStore().events.byId[id];
        if (existingEvent && existingEvent.detailed) {
            return new Promise((yes) => {
                yes(existingEvent);
            });
        }

        // Not yet fetched, fetch details
        dispatch(fetchingEventDetails(id));
        let promise = new Promise((yes, no) => {
            return fetch(`${ API_URL }v1/events/${ id }/`).then((response) => {
                if (!response.ok) {
                    return no();
                }
                response.json().then((rawEvent: Object) => {
                    let event = {
                        ...rawEvent,
                        detailed: true,
                        id: String(rawEvent.id),
                        externalId: String(rawEvent.external_id),
                        ticketCost: rawEvent.ticket_cost,
                        ticketCount: rawEvent.ticket_count,
                        ticketMax: rawEvent.ticket_max,
                        ticketMaxPerUser: rawEvent.ticket_max_per_user,
                        ticketMin: rawEvent.ticket_min
                    };
                    yes(event);
                }, no);
            }, no);
        });

        promise.then((event) => {
            dispatch(fetchedEventDetails(event));
        }, () => {
            dispatch(errorFetchingEventDetails());
        });

        return promise;
    }
}

export function fetchEvents() {
    return (dispatch: Function) => {
        dispatch(fetchingBasicEvents());

        let promise = new Promise((yes, no) => {
            return fetch(`${ API_URL }v1/fantasy/events/`).then((response) => {
                response.json().then((rawEvents: Array<Object>) => {
                    let events: Array<Object> = rawEvents.objects.map((rawEvent: Object) => {
                        return {
                            id: String(rawEvent.i),
                            description: rawEvent.d,
                            closeTimestamp: rawEvent.ct,
                            closeMoment: moment.unix(rawEvent.ct),
                            status: rawEvent.s,
                            context: rawEvent.ctx,
                            ticketMax: rawEvent.max,
                            ticketMaxPerUser: rawEvent.maxu,
                            ticketMin: rawEvent.min,
                            ticketCount: rawEvent.tcc || 0,
                            ticketCost: rawEvent.tc,
                            payout: rawEvent.p,
                            externalId: rawEvent.eid,
                            adminId: rawEvent.adm,
                            allowedProvinces: rawEvent.ep ? JSON.parse(rawEvent.ep) : [],
                            restrictedProvinces: rawEvent.rp ? JSON.parse(rawEvent.rp) : [],

                            featured: rawEvent.f === 'true', // TODO Check this is correct
                        };
                    });
                    yes(events);
                }, no);
            }, no);
        })

        promise.then((events) => {
            dispatch(fetchedBasicEvents(events));
        }, () => {
            dispatch(errorFetchingBasicEvents());
        });

        return promise;
    };
}
