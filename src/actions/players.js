export const FETCHED_PLAYERS = 'FETCHED_PLAYERS';


export function fetchedPlayers(players) {
    return {
        type: FETCHED_PLAYERS,
        players
    };
}
