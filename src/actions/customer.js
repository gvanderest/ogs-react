import Promise from 'promise';
import { API_URL } from '../settings';


export const FETCHING_ACTIVE_CUSTOMER = 'FETCHING_ACTIVE_CUSTOMER';
export const FETCHED_ACTIVE_CUSTOMER = 'FETCHED_ACTIVE_CUSTOMER';
export const ERROR_FETCHING_ACTIVE_CUSTOMER = 'ERROR_FETCHING_ACTIVE_CUSTOMER';
export const LOGOUT_CUSTOMER = 'LOGOUT_CUSTOMER';
export const AUTHENTICATING_CUSTOMER = 'AUTHENTICATING_CUSTOMER';
export const ERROR_AUTHENTICATING_CUSTOMER = 'ERROR_AUTHENTICATING_CUSTOMER';
export const AUTHENTICATED_CUSTOMER = 'AUTHENTICATED_CUSTOMER';


export function fetchingActiveCustomer() {
    return {
        type: FETCHING_ACTIVE_CUSTOMER
    };
}


export function fetchedActiveCustomer(customer) {
    return {
        type: FETCHED_ACTIVE_CUSTOMER,
        customer
    }
}


export function errorFetchingActiveCustomer() {
    return {
        type: ERROR_FETCHING_ACTIVE_CUSTOMER
    }
}


export function logoutCustomer() {
    return (dispatch) => {
        fetch(`${ API_URL }v1/auth`, {
            method: 'DELETE',
            credentials: 'include'
        }).then(() => {
            dispatch(fetchedActiveCustomer(null));
        }, () => {
            dispatch(fetchedActiveCustomer(null));
        });
    };
}


export function fetchActiveCustomer() {
    return (dispatch) => {
        let promise = new Promise((yes, no) => {
            dispatch(fetchingActiveCustomer());

            return fetch(`${ API_URL }v1/auth/`, {
                credentials: 'include'
            }).then((response) => {
                response.json().then((authCustomerResponse) => {
                    if (authCustomerResponse.objects.length) {
                        let authCustomer = authCustomerResponse.objects[0];
                        let csrfToken = authCustomer['X-CSRFToken'];
                        fetch(`${ API_URL}v1/customers/${ authCustomer.id }/`, {
                            credentials: 'include'
                        }).then((customerResponse) => {
                            customerResponse.json().then((rawCustomer) => {
                                // FIXME do some cleanup of the Customer here
                                let customer = {
                                    ...rawCustomer,
                                    csrfToken,
                                    settings: rawCustomer.settings_json
                                }
                                yes(customer);
                            }, no);
                        }, no);
                    } else {
                        yes();
                    }
                }, no);
            }, no);
        });

        promise.then((customer) => {
            dispatch(fetchedActiveCustomer(customer));
        }, () => {
            dispatch(errorFetchingActiveCustomer());
        });

        return promise;
    };
}


export function authenticatingCustomer() {
    return {
        type: AUTHENTICATING_CUSTOMER
    };
}


export function errorAuthenticatingCustomer() {
    return {
        type: ERROR_AUTHENTICATING_CUSTOMER
    };
}


export function authenticatedCustomer(customer) {
    return {
        type: AUTHENTICATED_CUSTOMER,
        customer
    };
}


/**
 * Login as a Customer.
 * @param {Object} data
 * @param {string} data.username
 * @param {string} data.password
 * @returns {Promise}
 */
export function authenticateCustomer(data) {
    return (dispatch) => {
        let promise = new Promise((yes, no) => {
            dispatch(authenticatingCustomer());
            fetch(`${ API_URL }v1/auth/`, {
                method: 'POST',
                credentials: 'include',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(data)
            }).then((response) => {
                response.json().then((customer) => {
                    yes(customer);
                }, no);
            }, no)
        });

        promise.then((customer) => {
            dispatch(authenticatedCustomer(customer));
            dispatch(fetchActiveCustomer());
        }, () => {
            dispatch(errorAuthenticatingCustomer());
        });

        return promise;
    };
}
