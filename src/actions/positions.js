export const FETCHED_POSITIONS = 'FETCHED_POSITIONS';


export function fetchedPositions(positions) {
    return {
        type: FETCHED_POSITIONS,
        positions
    };
}
