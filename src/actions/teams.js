export const FETCHED_TEAMS = 'FETCHED_TEAMS';


export function fetchedTeams(teams) {
    return {
        type: FETCHED_TEAMS,
        teams
    };
}
