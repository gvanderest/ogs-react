import moment from 'moment';
import 'moment-timezone';
import { MOMENT_TIMEZONE, MOMENT_DATE_FORMAT } from '../settings';


export default function formatDate(dateMoment) {
    if (!dateMoment) {
        return '';
    }
    return moment(dateMoment).tz(MOMENT_TIMEZONE).format(MOMENT_DATE_FORMAT);
}
