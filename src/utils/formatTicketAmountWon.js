import formatOgsDollar from './formatOgsDollar';


export default function formatTicketAmountwon(amount) {
    if (!amount) {
        return '--';
    }
    return formatOgsDollar(amount, { hideZeroCents: true });
}
