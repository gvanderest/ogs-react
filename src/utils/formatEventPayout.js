import formatOgsDollar from './formatOgsDollar';


export default function formatEventPayout(amount) {
    return formatOgsDollar(amount, { hideZeroCents: true });
}
