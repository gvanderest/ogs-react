import formatOgsDollar from './formatOgsDollar';
import translate from './translate';


export default function formatTicketCost(amount) {
    if (amount === 0) {
        return translate('FREE');
    }
    return formatOgsDollar(amount, { hideZeroCents: true });
}
