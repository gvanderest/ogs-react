const defaultOptions = {
    hideCents: false,
    hideZeroCents: false,
    zeroValueOutput: undefined
};


export default function formatDollar(amount, options=defaultOptions) {

    if (amount === 0 && options.zeroValueOutput !== undefined) {
        return options.zeroValueOutput;
    }

    let negative = amount < 0;
    let letters = Math.abs(amount).toFixed(2);
    let parts = letters.split('.');
    let [dollars, cents] = parts;

    let formattedDollar = '';
    while (dollars.length > 3) {
        formattedDollar = ',' + dollars.slice(dollars.length - 3) + formattedDollar;
        dollars = dollars.slice(0, dollars.length - 3);
    }
    formattedDollar = dollars + formattedDollar;

    let output = [formattedDollar];

    if (!(options.hideCents || cents === '00' && options.hideZeroCents)) {
        output.push(cents);
    }

    return (negative ? '-' : '') + '$' + output.join('.');
}
