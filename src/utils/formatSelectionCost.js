import formatDollar from './formatDollar';


export default function formatSelectionCost(amount) {
    return formatDollar(amount / 100, { hideCents: true });
}
