import formatDollar from './formatDollar';


export default function formatOgsDollar(amount, options) {
    return formatDollar(amount / 100, options);
}
