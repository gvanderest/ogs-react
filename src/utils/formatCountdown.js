import moment from 'moment';
import _ from 'lodash';


const ONE_SECOND = 1;
const ONE_MINUTE = ONE_SECOND * 60;
const ONE_HOUR = ONE_MINUTE * 60;
const ONE_DAY = ONE_HOUR * 24;


export default function formatCountdown(dateMoment) {
    if (!dateMoment) {
        return '';
    }

    let now = moment();
    let distance = dateMoment.diff(now, 'seconds');

    if (distance <= 0) {
        return 'LIVE';
    }

    if (distance >= ONE_DAY) {
        let days = Math.floor(distance / ONE_DAY);
        return days + ' DAY' + (Math.abs(days) !== 1 ? 'S' : '');
    }

    let units = [ONE_HOUR, ONE_MINUTE, ONE_SECOND];
    let parts = [];

    let remainder = distance;
    units.forEach((unit) => {
        let count = Math.floor(remainder / unit);
        parts.push(count);
        remainder -= count * unit;
    });
    parts = _.map(parts, (part) => {
        return _.padStart(part, 2, '0');
    });
    return parts.join(':');
}
