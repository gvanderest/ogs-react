export default function formatPoints(rawPoints) {
    return (rawPoints / 100).toFixed(2);
}
