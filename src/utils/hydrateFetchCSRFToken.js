import { store } from '../redux';


export default function hydrateFetchCSRFToken(options={}) {
    if (!options.headers) {
        options.headers = {};
    }
    let state = store.getState();
    let customer = state.customer.activeCustomer;
    if (customer) {
        options.headers['x-csrftoken'] = customer.csrfToken;
    }

    return options;
}
