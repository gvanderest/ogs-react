import React from 'react';


export default class YouTube extends React.Component {
    render() {
        return (
            <iframe
                allowFullScreen=""
                frameBorder="0"
                height={ this.props.height || '315' }
                src={ `https://www.youtube.com/embed/${ this.props.video }?rel=0&amp;wmode=transparent` }
                width={ this.props.width || '100%' }
            ></iframe>
        );

    }
}

YouTube.propTypes = {
    video: React.PropTypes.string.isRequired
}
