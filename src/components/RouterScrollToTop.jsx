import React from 'react';
import { withRouter } from 'react-router'

class RouterScrollToTop extends React.Component {
    componentDidUpdate(prevProps) {
        if (this.props.location !== prevProps.location) {
            window.scrollTo(0, 0)
        }
    }
    render() {
        return (
            <div>{ this.props.children }</div>
        );
    }
}
RouterScrollToTop.propTypes = {
    location: React.PropTypes.object,
    children: React.PropTypes.array
};


export default withRouter(RouterScrollToTop);
